package fr.umlv.chatos.reader;

import fr.umlv.chatos.packet.*;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FrameReaderTest {

    @Test
    public void login(){
        var string = "Ps€udonyme";
        var bb = ByteBuffer.allocate(1024);
        var LoginBytes = StandardCharsets.UTF_8.encode(string);
        bb.put((byte) 0).putInt(LoginBytes.remaining()).put(LoginBytes);
        FrameReader cr = new FrameReader();
        Frame frame;
        assertEquals(Reader.ProcessStatus.DONE, cr.process(bb));
        frame = cr.get();
        //assertEquals(Frame.Operands.LOGIN, frame.getOpcode());
        assertEquals(0, bb.position());
        assertEquals(bb.capacity(),bb.limit());
    }

    @Test
    public void message(){
        var string = "Ps€udonyme";
        var string2 = "Voici un test de message";
        var string3 = "Alice";
        var string4 = "Other m€ssage";
        var bb = ByteBuffer.allocate(1024);
        var bytes = StandardCharsets.UTF_8.encode(string);
        var bytes2 = StandardCharsets.UTF_8.encode(string2);
        var bytes3 = StandardCharsets.UTF_8.encode(string3);
        var bytes4 = StandardCharsets.UTF_8.encode(string4);
        // First message
        bb.put((byte) 3).putInt(bytes.remaining()).put(bytes).putInt(bytes2.remaining()).put(bytes2);
        // Second message
        bb.put((byte) 3).putInt( bytes3.remaining()).put(bytes3).putInt(bytes4.remaining()).put(bytes4);
        FrameReader cr = new FrameReader();
        Frame frame;
        assertEquals(Reader.ProcessStatus.DONE, cr.process(bb));
        frame = cr.get();
        //assertEquals(Frame.Operands.MESSAGE, frame.getOpcode());
        assertEquals(29, bb.position());
        assertEquals(bb.capacity(),bb.limit());
        Message message = new Message(string, string2);
        assertEquals("Ps€udonyme",message.getRequester());
        assertEquals("Voici un test de message",message.getText());
        cr.reset();
        assertEquals(Reader.ProcessStatus.DONE,cr.process(bb));
        assertEquals(0,bb.position());
        assertEquals(bb.capacity(),bb.limit());
        Message message2 = new Message(string3, string4);
        assertEquals("Alice",message2.getRequester());
        assertEquals("Other m€ssage",message2.getText());
    }

    @Test
    public void messagePrivate(){
        var string = "Alice";
        var string2 = "Bob";
        var string3 = "Voici un test de message";
        var bb = ByteBuffer.allocate(1024);
        var bytes = StandardCharsets.UTF_8.encode(string);
        var bytes2 = StandardCharsets.UTF_8.encode(string2);
        var bytes3 = StandardCharsets.UTF_8.encode(string3);
        bb.put((byte) 4).putInt(bytes.remaining()).put(bytes).putInt(bytes2.remaining()).put(bytes2).putInt(bytes3.remaining()).put(bytes3);
        FrameReader cr = new FrameReader();
        Frame frame;
        assertEquals(Reader.ProcessStatus.DONE, cr.process(bb));
        frame = cr.get();
        //assertEquals(Frame.Operands.MESSAGE_PRIVATE, frame.getOpcode());
        assertEquals(0,bb.position());
        assertEquals(bb.capacity(),bb.limit());
        MessagePrivate messagePrivate = new MessagePrivate(string, string2,string3);
        assertEquals("Alice", messagePrivate.getRequester());
        assertEquals("Bob", messagePrivate.getTarget());
        assertEquals("Voici un test de message", messagePrivate.getText());
    }

    @Test
    public void requestPrivate(){
        var string = "Alice";
        var string2 = "Bob";
        var bb = ByteBuffer.allocate(1024);
        var bytes = StandardCharsets.UTF_8.encode(string);
        var bytes2 = StandardCharsets.UTF_8.encode(string2);
        bb.put((byte) 5).putInt(bytes.remaining()).put(bytes).putInt(bytes2.remaining()).put(bytes2);
        FrameReader cr = new FrameReader();
        Frame frame;
        assertEquals(Reader.ProcessStatus.DONE, cr.process(bb));
        frame = cr.get();
        //assertEquals(Frame.Operands.REQUEST_PRIVATE, frame.getOpcode());
        assertEquals(0,bb.position());
        assertEquals(bb.capacity(),bb.limit());
        RequestPrivate requestPrivate = new RequestPrivate(string, string2);
        assertEquals("Alice", requestPrivate.getRequester());
        assertEquals("Bob", requestPrivate.getTarget());
    }

    @Test
    public void okPrivate(){
        var string = "Alice";
        var string2 = "Bob";
        var bb = ByteBuffer.allocate(1024);
        var bytes = StandardCharsets.UTF_8.encode(string);
        var bytes2 = StandardCharsets.UTF_8.encode(string2);
        bb.put((byte) 6).putInt(bytes.remaining()).put(bytes).putInt(bytes2.remaining()).put(bytes2);
        FrameReader cr = new FrameReader();
        Frame frame;
        assertEquals(Reader.ProcessStatus.DONE, cr.process(bb));
        frame = cr.get();
        //assertEquals(Frame.Operands.OK_PRIVATE, frame.getOpcode());
        assertEquals(0,bb.position());
        assertEquals(bb.capacity(),bb.limit());
        OkPrivate okPrivate = new OkPrivate(string, string2);
        assertEquals("Alice", okPrivate.getRequester());
        assertEquals("Bob", okPrivate.getTarget());
    }

    @Test
    public void koPrivate(){
        var string = "Alice";
        var string2 = "Bob";
        var bb = ByteBuffer.allocate(1024);
        var bytes = StandardCharsets.UTF_8.encode(string);
        var bytes2 = StandardCharsets.UTF_8.encode(string2);
        bb.put((byte) 7).putInt(bytes.remaining()).put(bytes).putInt(bytes2.remaining()).put(bytes2);
        FrameReader cr = new FrameReader();
        Frame frame;
        assertEquals(Reader.ProcessStatus.DONE, cr.process(bb));
        frame = cr.get();
        //assertEquals(Frame.Operands.KO_PRIVATE, frame.getOpcode());
        assertEquals(0,bb.position());
        assertEquals(bb.capacity(),bb.limit());
    }

    @Test
    public void idPrivate() {
        var string = "Alice";
        var string2 = "Bob";
        var connectId = new Random().nextLong();
        var bb = ByteBuffer.allocate(1024);
        var bytes = StandardCharsets.UTF_8.encode(string);
        var bytes2 = StandardCharsets.UTF_8.encode(string2);
        bb.put((byte) 8).putInt(bytes.remaining()).put(bytes).putInt(bytes2.remaining()).put(bytes2).putLong(connectId);
        FrameReader cr = new FrameReader();
        Frame frame;
        assertEquals(Reader.ProcessStatus.DONE, cr.process(bb));
        frame = cr.get();
        //assertEquals(Frame.Operands.ID_PRIVATE, frame.getOpcode());
        assertEquals(0,bb.position());
        assertEquals(bb.capacity(),bb.limit());
        IdPrivate idPrivate = new IdPrivate(string, string2, connectId);
        assertEquals("Alice", idPrivate.getRequester());
        assertEquals("Bob", idPrivate.getTarget());
        assertEquals(connectId, idPrivate.getConnectId());
    }

    @Test
    public void login_private() {
        var connectId = new Random().nextLong();
        var bb = ByteBuffer.allocate(1024);
        bb.put((byte) 9).putLong(connectId);
        FrameReader cr = new FrameReader();
        Frame frame;
        assertEquals(Reader.ProcessStatus.DONE, cr.process(bb));
        frame = cr.get();
        //assertEquals(Frame.Operands.LOGIN_PRIVATE, frame.getOpcode());
        assertEquals(0,bb.position());
        assertEquals(bb.capacity(),bb.limit());
        LoginPrivate loginPrivate = new LoginPrivate(connectId);
        assertEquals(connectId, loginPrivate.getConnectId());
    }

    @Test
    public void opcodesOneAfterAnother(){
        Frame frame;
        var bb = ByteBuffer.allocate(1024);
        bb.put((byte) 1).put((byte) 2).put((byte) 10);
        var cr = new FrameReader();
        assertEquals(Reader.ProcessStatus.DONE, cr.process(bb));
        frame = cr.get();
        //assertEquals(Frame.Operands.LOGIN_ACCEPTED, frame.getOpcode());
        cr.reset();
        assertEquals(Reader.ProcessStatus.DONE, cr.process(bb));
        frame = cr.get();
        //assertEquals(Frame.Operands.LOGIN_REFUSED, frame.getOpcode());
        cr.reset();
        assertEquals(Reader.ProcessStatus.DONE, cr.process(bb));
        frame = cr.get();
        //assertEquals(Frame.Operands.ESTABLISHED, frame.getOpcode());
    }

    //@Test
    public void smallBuffer(){
        var string = "\u20ACa\u20AC";
        var bb = ByteBuffer.allocate(1024);
        var bytes = StandardCharsets.UTF_8.encode(string);
        bb.putInt(bytes.remaining()).put(bytes).flip();
        var bbSmall = ByteBuffer.allocate(2);
        var sr = new FrameReader();
        while (bb.hasRemaining()) {
            while(bb.hasRemaining() && bbSmall.hasRemaining()){
                bbSmall.put(bb.get());
            }
            if (bb.hasRemaining()) {
                assertEquals(Reader.ProcessStatus.REFILL,sr.process(bbSmall));
            } else {
                assertEquals(Reader.ProcessStatus.DONE,sr.process(bbSmall));
            }
        }
        assertEquals(string,sr.get());
    }

    @Test
    public void errorGet(){
        var command = new FrameReader();
        assertThrows(IllegalStateException.class,() -> {var res=command.get();});
    }

    //@Test
    public void errorOpcode(){
        var command = new FrameReader();
        var bb = ByteBuffer.allocate(1024);
        var bytes = StandardCharsets.UTF_8.encode("aaaaa");
        bb.put((byte) 20 ).putInt(bytes.remaining()).put(bytes);
        assertEquals(Reader.ProcessStatus.ERROR,command.process(bb));
    }

    @Test
    public void errorTooBig(){
        var command = new FrameReader();
        var bb = ByteBuffer.allocate(1024);
        var bytes = StandardCharsets.UTF_8.encode("aaaaa");
        bb.putInt(1025).put(bytes);
        assertEquals(Reader.ProcessStatus.ERROR,command.process(bb));
    }

}
