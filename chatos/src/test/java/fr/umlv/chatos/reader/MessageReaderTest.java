package fr.umlv.chatos.reader;

import fr.umlv.chatos.packet.Message;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MessageReaderTest {

    @Test
    public void reset(){
        var string = "Ps€udonyme";
        var string2 = "Voici un test de message";
        var string3 = "Alice"; // Integer.BYTES : 4 + Alice : 5
        var string4 = "Other m€ssage"; // Integer.BYTES : 4 + m€ssage : 15
        // € Length of 3 encoded with UTF-8
        var bb = ByteBuffer.allocate(1024);
        var bytes = StandardCharsets.UTF_8.encode(string);
        var bytes2 = StandardCharsets.UTF_8.encode(string2);
        var bytes3 = StandardCharsets.UTF_8.encode(string3);
        var bytes4 = StandardCharsets.UTF_8.encode(string4);
        // First message
        bb.putInt(bytes.remaining()).put(bytes).putInt(bytes2.remaining()).put(bytes2);
        // Second message
        bb.putInt(bytes3.remaining()).put(bytes3).putInt(bytes4.remaining()).put(bytes4);
        MessageReader mr = new MessageReader();
        Message message;
        assertEquals(Reader.ProcessStatus.DONE,mr.process(bb));
        message = mr.get();
        assertEquals(string, message.getRequester());
        assertEquals(string2, message.getText());
        assertEquals(28,bb.position());
        assertEquals(bb.capacity(),bb.limit());
        mr.reset();
        assertEquals(Reader.ProcessStatus.DONE,mr.process(bb));
        message = mr.get();
        assertEquals(string3, message.getRequester());
        assertEquals(string4, message.getText());
        assertEquals(0,bb.position());
        assertEquals(bb.capacity(),bb.limit());
    }

    @Test
    public void smallBuffer(){
        /*
        var string = "\u20ACa\u20AC";
        var bb = ByteBuffer.allocate(1024);
        var bytes = StandardCharsets.UTF_8.encode(string);
        bb.putInt(bytes.remaining()).put(bytes).flip();
        var bbSmall = ByteBuffer.allocate(2);
        var sr = new MessageReader();
        while (bb.hasRemaining()) {
            while(bb.hasRemaining() && bbSmall.hasRemaining()){
                bbSmall.put(bb.get());
            }
            if (bb.hasRemaining()) {
                assertEquals(Reader.ProcessStatus.REFILL,sr.process(bbSmall));
            } else {
                assertEquals(Reader.ProcessStatus.DONE,sr.process(bbSmall));
            }
        }
        assertEquals(string,sr.get());
        */
    }

    @Test
    public void errorGet(){
        var sr = new MessageReader();
        assertThrows(IllegalStateException.class,() -> {var res=sr.get();});
    }

    @Test
    public void errorNeg(){
        var sr = new MessageReader();
        var bb = ByteBuffer.allocate(1024);
        var bytes = StandardCharsets.UTF_8.encode("aaaaa");
        bb.putInt(-1).put(bytes);
        assertEquals(Reader.ProcessStatus.ERROR,sr.process(bb));
    }

    @Test
    public void errorTooBig(){
        var sr = new MessageReader();
        var bb = ByteBuffer.allocate(1024);
        var bytes = StandardCharsets.UTF_8.encode("aaaaa");
        bb.putInt(1025).put(bytes);
        assertEquals(Reader.ProcessStatus.ERROR,sr.process(bb));
    }
}
