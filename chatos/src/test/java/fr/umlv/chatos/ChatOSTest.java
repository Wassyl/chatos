package fr.umlv.chatos;


import fr.umlv.chatos.reader.FrameReaderTest;
import fr.umlv.chatos.reader.MessageReaderTest;
import fr.umlv.chatos.reader.StringReaderTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ ChatOSTest.ReaderTest.class })
public class ChatOSTest {

    @RunWith(Suite.class)
    @Suite.SuiteClasses({ StringReaderTest.class, MessageReaderTest.class, FrameReaderTest.class })
    public class ReaderTest {

    }

}
