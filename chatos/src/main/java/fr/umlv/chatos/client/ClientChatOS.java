package fr.umlv.chatos.client;

import fr.umlv.chatos.context.client.ClientContext;
import fr.umlv.chatos.context.client.ClientPrivateContext;
import fr.umlv.chatos.context.client.ClientPublicContext;
import fr.umlv.chatos.packet.*;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.nio.channels.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import static fr.umlv.chatos.packet.Frame.convertToByteBuffer;

/**
 * Client Chat OS is the class representing a client in the Chat OS
 */
public class ClientChatOS {

    /**
     * Logger to announce info, warning or error
     */
    private static final Logger logger = Logger.getLogger(ClientChatOS.class.getName());

    /**
     * Define the charset UTF_8
     */
    private static final Charset UTF_8 = StandardCharsets.UTF_8;

    /**
     * The socket Channel
     */
    private final SocketChannel sc;

    /**
     * The selector
     */
    private final Selector selector;

    /**
     * The inetSocketAdress of the server
     */
    private final InetSocketAddress serverAddress;

    /**
     * The login
     */
    private final String login;

    /**
     * The thread Console
     */
    private final Thread console;

    /**
     * A queue to stock command
     */
    private final ArrayBlockingQueue<String> commandQueue = new ArrayBlockingQueue<>(10);

    /**
     * A map to stock the other client with the Private context of the actual client and his connectID
     */
    private final HashMap<String, ClientPrivateContext> privateConnections = new HashMap<>();

    /**
     * A list to stock all clients that request a connection with another in private connection
     */
    private final ArrayList<String> requesters = new ArrayList<>();

    /**
     * an Object to synchronized some methods
     */
    private final Object lock = new Object();

    /**
     * Create the context when the Client is in the public connection
     */
    private ClientPublicContext uniqueContext;

    /**
     * The constructor for a client in Chat OS. A client in the Chat OS need a login and a serverAddress.
     * @param login Username
     * @param serverAddress
     * @throws IOException
     */
    private ClientChatOS(String login, InetSocketAddress serverAddress) throws IOException {
        Objects.requireNonNull(login, "login cannot be null");
        Objects.requireNonNull(serverAddress, "serverAddress cannot be null");
        logger.info("Client started on port " + serverAddress.getPort());
        this.serverAddress = serverAddress;
        this.login = login;
        this.sc = SocketChannel.open();
        this.selector = Selector.open();
        this.console = new Thread(this::consoleRun);
    }

    /**
     * Generate a new Private Context with the connectId.
     * @param connectId
     * @return the new Context of the client with his connectID
     * @throws IOException
     */
    private ClientPrivateContext generatePrivateContext(String otherClient, long connectId) throws IOException {
        var tmpSc = SocketChannel.open();
        tmpSc.configureBlocking(false);
        var key = tmpSc.register(selector, SelectionKey.OP_CONNECT);
        var context = new ClientPrivateContext(key, this, otherClient, connectId);
        key.attach(context);
        tmpSc.connect(serverAddress);
        return context;
    }

    /**
     * Add to the Hashmap Private Connection a new client with the context of the other with the connectId.
     * @param otherClient
     * @param connectId
     */
    public void addPrivateConnection( String otherClient, long connectId ) {
        try {
            logger.info("Added private connection for " + otherClient);
            privateConnections.put(otherClient, generatePrivateContext(otherClient, connectId));
        } catch (IOException e) {
            logger.severe("Couldn't add private connection for - " + otherClient);
        }
    }

    /**
     * remove the otherClient to close the privateConnection for him
     * @param otherClient
     */
    public void closePrivateConnectionFor(String otherClient) {
        privateConnections.remove(otherClient);
    }

    /**
     * Add to the list Requesters a new requester
     * @param requester
     */
    public void addRequester(String requester) {
        requesters.add(requester);
    }

    /**
     * Try to read the console to know the command and send it
     */
    private void consoleRun() {
        try {
            var scan = new Scanner(System.in);
            while( !Thread.interrupted() ) {
                while (scan.hasNextLine()) {
                    var msg = scan.nextLine();
                    if( !msg.isEmpty() ){
                        sendCommand(msg);
                    }
                }
            }
        } catch (InterruptedException e) {
            logger.info("Console thread has been interrupted");
        } finally {
            logger.info("Console thread stopped");
        }
    }

    /**
     * Send a command to the selector via commandQueue and wakes it up
     *
     * @param msg Command the user just wrote on standard input
     * @throws InterruptedException
     */
    private void sendCommand(String msg) throws InterruptedException {
        synchronized (lock) {
            commandQueue.put(msg);
            selector.wakeup();
        }
    }

    /**
     * Processes the command from commandQueue
     */
    private void processCommands() {
        synchronized (lock) {
            String command;
            while( (command = commandQueue.poll()) != null ) {
                if( command.equals("SHUTDOWN") ) {
                    close();
                    return;
                }
                switch (command.charAt(0)){
                    case '@' -> {
                        var firstSpace = command.indexOf(' ');
                        if( firstSpace == -1 ){ // There are no messages
                            continue;
                        }
                        var target = command.substring(1, firstSpace); // @Test bonjour
                        uniqueContext.queueMessage(new MessagePrivate(login, target, command.substring(firstSpace+1)).asByteBuffer());
                    }
                    case '/' -> {
                        var firstSpace = command.indexOf(' ');
                        if( firstSpace == -1 ){ // There are no messages
                            var target = command.substring(1);
                            // We close the private connection if it exists
                            if( privateConnections.get(target) != null ) {
                                logger.info("Closing private connection with " + target);
                                privateConnections.get(target).close();
                            }
                            continue;
                        }
                        // "/target LeMessage" -> remove '/'
                        var target = command.substring(1, firstSpace);
                        // Everything else after the first space
                        var message = command.substring(firstSpace+1);
                        // Enqueue the message when the connection exists
                        if( privateConnections.get(target) != null  ) {
                            privateConnections.get(target).queueMessage( new PrivatePacket( convertToByteBuffer(message) ).asByteBuffer() );
                        } else {
                            // Else, we simply initiate it
                            negotiateConnection( target, message );
                        }
                    }
                    // MESSAGE  - Opcode 3 - Message broadcast to every user
                    default -> uniqueContext.queueMessage(new Message(login, command).asByteBuffer());
                }
            }
        }
    }

    /**
     * Given a specific user, will enqueue a RequestPrivate to initiate a private connection
     * @param target
     * @param message
     */
    private void negotiateConnection(String target, String message) {
        // Checking if somebody is trying to establish a connection with us
        if( requesters.contains(target) ){ // Target is requesting you to open a private connection !
            // - OkPrivate - Opcode 6 - If the user agrees, we establish the connection
            if( message.equals("OK") ) {
                uniqueContext.queueMessage(new OkPrivate(target, login).asByteBuffer());
            } else {
                System.out.println("You refused " + target + "'s request");
                // - KoPrivate - Opcode 7 - Else, we inform the requester it was denied
                uniqueContext.queueMessage(new KoPrivate(target, login).asByteBuffer());
            }
            requesters.remove(target);
        } else {
            // - RequestPrivate - Opcode 5 - Else  We are trying to engage the private connection
            uniqueContext.queueMessage(new RequestPrivate(login, target).asByteBuffer());
        }
    }

    /**
     * Launch the new client
     * @throws IOException
     */
    public void launch() throws IOException {
        sc.configureBlocking(false);
        var key = sc.register(selector, SelectionKey.OP_CONNECT);
        uniqueContext = new ClientPublicContext(key, this);
        key.attach(uniqueContext);
        sc.connect(serverAddress);

        console.start();

        System.out.println("Press SHUTDOWN to stop the program");
        while(!Thread.interrupted()) {
            try {
                printKeys(); // for debug
                selector.select(this::treatKey);
                processCommands();
            } catch (UncheckedIOException tunneled) {
                throw tunneled.getCause();
            } catch (IOException io) {
                logger.severe("Client shutting down");
                return;
            }
        }
        System.out.println("See you next time !");
    }

    /**
     * Treat the SelectionKey
     * @param key
     */
    private void treatKey(SelectionKey key) {
        try {
            if (key.isValid() && key.isConnectable()) {
                ((ClientContext) key.attachment()).doConnect();
            }
        } catch(IOException ioe) {
            // lambda call in select requires to tunnel IOException
            throw new UncheckedIOException(ioe);
        }
        try {
            if (key.isValid() && key.isWritable()) {
                ((ClientContext) key.attachment()).doWrite();
            }
            if (key.isValid() && key.isReadable()) {
                ((ClientContext) key.attachment()).doRead();
            }
        } catch (IOException e) {
            logger.log(Level.INFO,"Connection closed with server due to IOException",e);
            silentlyClose(key);
        }
    }

    /**
     * Close a SocketChannel
     * @param key
     */
    private void silentlyClose(SelectionKey key) {
        var sc = (Channel) key.channel();
        try {
            sc.close();
        } catch (IOException e) {
            // ignore exception
        }
    }

    /**
     * Close a client
     */
    public void close() {
        logger.info("Closing client");
        try {
            selector.keys().forEach( this::silentlyClose );
            selector.close();
            //console.interrupt(); // Doesn't work
            System.exit(-1);
        } catch (IOException e) {
            logger.severe("IOException occurred when closing client");
        }
    }

    /**
     * Get the login of a client
     * @return login of the client
     */
    public String getLogin() {
        return login;
    }

    /**
     * Main : to launch a client
     * @param args
     * @throws NumberFormatException
     * @throws IOException
     */
    public static void main(String[] args) throws NumberFormatException, IOException {
        if (args.length!=3){
            usage();
            return;
        }
        new ClientChatOS(args[0],new InetSocketAddress(args[1],Integer.parseInt(args[2]))).launch();
    }

    /**
     * How to lunch the client in the console
     */
    private static void usage(){
        System.out.println("ClientChatOS host port username");
        System.out.println("Username length must be lower than 30 bytes ( UTF8 encoding use )");
    }

    /***
     *  Theses methods are here to help understanding the behavior of the selector
     ***/
    private String interestOpsToString(SelectionKey key){
        if (!key.isValid()) {
            return "CANCELLED";
        }
        int interestOps = key.interestOps();
        ArrayList<String> list = new ArrayList<>();
        if ((interestOps&SelectionKey.OP_ACCEPT)!=0) list.add("OP_ACCEPT");
        if ((interestOps&SelectionKey.OP_READ)!=0) list.add("OP_READ");
        if ((interestOps&SelectionKey.OP_WRITE)!=0) list.add("OP_WRITE");
        return String.join("|",list);
    }

    /**
     * print keys of the selector according if is the server socket channel or the client.
     */
    public void printKeys() {
        Set<SelectionKey> selectionKeySet = selector.keys();
        if (selectionKeySet.isEmpty()) {
            System.out.println("The selector contains no key : this should not happen!");
            return;
        }
        System.out.println("The selector contains:");
        for (SelectionKey key : selectionKeySet){
            SelectableChannel channel = key.channel();
            if (channel instanceof ServerSocketChannel) {
                System.out.println("\tKey for ServerSocketChannel : "+ interestOpsToString(key));
            } else {
                SocketChannel sc = (SocketChannel) channel;
                System.out.println("\tKey for Client "+ remoteAddressToString(sc) +" : "+ interestOpsToString(key));
            }
        }
    }

    /**
     * transform the remote address to string
     * @param sc
     * @return a string of the remote address
     */
    private String remoteAddressToString(SocketChannel sc) {
        try {
            return sc.getRemoteAddress().toString();
        } catch (IOException e){
            return "???";
        }
    }

    /**
     * Print the key of the channel according if is the server socket channel or the client
     * @param key
     */
    public void printSelectedKey(SelectionKey key) {
        SelectableChannel channel = key.channel();
        if (channel instanceof ServerSocketChannel) {
            System.out.println("\tServerSocketChannel can perform : " + possibleActionsToString(key));
        } else {
            SocketChannel sc = (SocketChannel) channel;
            System.out.println("\tClient " + remoteAddressToString(sc) + " can perform : " + possibleActionsToString(key));
        }
    }

    /**
     * String of possible actions according to the selection key
     * @param key
     * @return string of possible actions according to the selection key
     */
    private String possibleActionsToString(SelectionKey key) {
        if (!key.isValid()) {
            return "CANCELLED";
        }
        ArrayList<String> list = new ArrayList<>();
        if (key.isAcceptable()) list.add("ACCEPT");
        if (key.isReadable()) list.add("READ");
        if (key.isWritable()) list.add("WRITE");
        return String.join(" and ",list);
    }
}
