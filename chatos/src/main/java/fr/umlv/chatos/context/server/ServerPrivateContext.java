package fr.umlv.chatos.context.server;

import fr.umlv.chatos.packet.Frame;
import fr.umlv.chatos.reader.PrivatePacketReader;
import fr.umlv.chatos.server.ServerChatOS;
import fr.umlv.chatos.visitor.ServerFramePrivateVisitor;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Logger;

/**
 * Creating a server context in a private connection
 */
public class ServerPrivateContext implements ServerContext {

    /**
     * Logger to announce info, warning or error
     */
    private static final Logger logger = Logger.getLogger(ServerPrivateContext.class.getName());

    /**
     * The SelectionKey
     */
    private final SelectionKey key;

    /**
     * The SocketChannel
     */
    private final SocketChannel sc;

    /**
     * Bytebuffer of the reader
     */
    private final ByteBuffer bbin = ByteBuffer.allocateDirect(BUFFER_SIZE);

    /**
     * Bytebuffer of the sender
     */
    private final ByteBuffer bbout = ByteBuffer.allocateDirect(BUFFER_SIZE);

    /**
     * The queue to stock the message in a bytebuffer
     */
    private final Queue<ByteBuffer> queue = new LinkedList<>(); // read-mode

    /**
     * The ByteReader to read a byte
     */
    private final PrivatePacketReader privatePacketReader = new PrivatePacketReader();

    private final ServerFramePrivateVisitor serverFrameVisitor;

    /**
     * The Server for the ChatOS
     */
    private final ServerChatOS server;

    /**
     * The connectId for the private connection
     */
    private final long connectId;

    /**
     * boolean to specify the connection state
     */
    private boolean closed = false;

    /**
     * Initialize the ServerPrivateContext
     * @param server ServerChatOS instance
     * @param key key used for the private connection
     * @param connectId Token linked to this private connection
     */
    public ServerPrivateContext(ServerChatOS server, SelectionKey key, long connectId){
        this.server = server;
        this.connectId = connectId;
        this.key = key;
        this.sc = (SocketChannel) key.channel();
        this.serverFrameVisitor = new ServerFramePrivateVisitor(server, this);
    }

    /**
     * Getter to get the key
     * @return its SelectionKey
     */
    public SelectionKey getKey() {
        return key;
    }

    /**
     * Getter to get the connectID
     * @return The connectId
     */
    public Long getConnectId() {
        return connectId;
    }

    /**
     * Process the content of bbin
     * The convention is that bbin is in write-mode before the call
     * to process and after the call
     */
    private void processIn() {
        // Transfer bytes to other client
        switch ( privatePacketReader.process(bbin) ) {
            case DONE -> {
                var privatePacket = privatePacketReader.get();
                privatePacketReader.reset();
                treatFrame(privatePacket);
            }
            case REFILL -> {}
            case ERROR -> logger.warning("Corrupted message received");
        }
    }

    /**
     * Treat a Frame packet freshly received
     * @param frame Frame to be computed
     */
    private void treatFrame(Frame frame) {
        frame.accept(serverFrameVisitor);
    }

    /**
     * Try to fill bbout from the message queue
     */
    private void processOut() {
        while (!queue.isEmpty()){
            var bb = queue.peek();
            if ( bb.remaining() <= bbout.remaining() ){
                queue.remove();
                bbout.put(bb);
            } else {
                break;
            }
        }
    }

    /**
     * Add a message to the message queue, tries to fill bbOut and updateInterestOps
     *
     * @param bb ByteBuffer to enqueue
     */
    public void queueMessage(ByteBuffer bb) {
        queue.add(bb);
        processOut();
        updateInterestOps();
    }

    /**
     * Update the interestOps of the key looking
     * only at values of the boolean closed and
     * of both ByteBuffers.
     *
     * The convention is that both buffers are in write-mode before the call
     * to updateInterestOps and after the call.
     * Also it is assumed that process has been be called just
     * before updateInterestOps.
     */
    private void updateInterestOps() {
        var intOps = 0;
        if( !closed && bbin.hasRemaining() ){ //
            intOps |= SelectionKey.OP_READ;
        }
        if( bbout.position() != 0  ) { // Hmmm
            intOps |= SelectionKey.OP_WRITE;
        }
        if( intOps == 0 ){
            logger.info("Closing context");
            silentlyClose();
            return;
        }
        key.interestOps(intOps);
    }

    /**
     * Performs the read action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doRead and after the call
     *
     * @throws IOException From read()
     */
    public void doRead() throws IOException {
        if( sc.read(bbin) == -1 ){
            logger.info("Detected read == -1 !");
            closed = true;
            server.closePrivateConnectionFor(connectId);
            return;
        }
        processIn();
        updateInterestOps();
    }

    /**
     * Performs the write action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doWrite and after the call
     *
     * @throws IOException from write()
     */
    public void doWrite() throws IOException {
        bbout.flip();
        sc.write(bbout);
        bbout.compact();
        processOut();
        updateInterestOps();
    }

    /**
     * Close the socketChannel
     */
    public void silentlyClose() {
        try {
            sc.close();
        } catch (IOException e) {
            // ignore exception
        }
    }
}
