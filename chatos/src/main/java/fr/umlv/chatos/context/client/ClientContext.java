package fr.umlv.chatos.context.client;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * An interface for the context of a client
 * All methods to do in the ClientPrivateContext and ClientPublicContext
 */
public interface ClientContext {

    /**
     * Define the size of the buffer
     */
    int BUFFER_SIZE = 1024;

    /**
     * Connection to the server
     * @throws IOException
     */
    void doConnect() throws IOException;

    /**
     * Performs the read action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doRead and after the call
     *
     * @throws IOException
     */
    void doRead() throws IOException;

    /**
     * Performs the write action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doWrite and after the call
     *
     * @throws IOException
     */
    void doWrite() throws IOException;

    void silentlyClose();
}
