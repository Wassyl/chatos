package fr.umlv.chatos.context.server;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public interface ServerContext {

    /**
     * DEFINE CHARSET UTF8
     */
    Charset UTF_8 = StandardCharsets.UTF_8;

    /**
     * Define the size of the buffer
     */
    int BUFFER_SIZE = 1024;

    /**
     * Add a message to the message queue, tries to fill bbOut and updateInterestOps
     *
     * @param bb
     */
    void queueMessage(ByteBuffer bb);

    /**
     * Performs the read action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doRead and after the call
     *
     * @throws IOException
     */
    void doRead() throws IOException;

    /**
     * Performs the write action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doWrite and after the call
     *
     * @throws IOException
     */
    void doWrite() throws IOException;

    void silentlyClose();

    SelectionKey getKey();
}
