package fr.umlv.chatos.context.server;

import fr.umlv.chatos.packet.*;
import fr.umlv.chatos.reader.FrameReader;
import fr.umlv.chatos.server.ServerChatOS;
import fr.umlv.chatos.visitor.ServerFrameVisitor;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.*;
import java.util.logging.Logger;

/**
 * Creating a server context in a public connection
 */
public class ServerPublicContext implements ServerContext {

    /**
     * Logger to announce info, warning or error
     */
    private static final Logger logger = Logger.getLogger(ServerPublicContext.class.getName());

    /**
     * The SelectionKey
     */
    private final SelectionKey key;

    /**
     * The SocketChannel
     */
    private final SocketChannel sc;

    /**
     * Bytebuffer of the reader
     */
    private final ByteBuffer bbin = ByteBuffer.allocateDirect(BUFFER_SIZE);

    /**
     * Bytebuffer of the sender
     */
    private final ByteBuffer bbout = ByteBuffer.allocateDirect(BUFFER_SIZE);

    /**
     * The queue to stock the message in a bytebuffer
     */
    private final Queue<ByteBuffer> queue = new LinkedList<>(); // read-mode

    /**
     * The Server for the ChatOS
     */
    private final ServerChatOS server;

    /**
     * The commandReader to read the command logged, not logged
     */
    private final FrameReader frameReader = new FrameReader();

    private final ServerFrameVisitor serverFrameVisitor;

    /**
     * Boolean closed to verify if the connection is close or not
     */
    private boolean closed = false;

    /**
     * Login of the client
     */
    private String login;

    /**
     * Initialize the Server Public Context
     * @param server ServerChatOS instance that creates the context
     * @param key Key used for selection
     */
    public ServerPublicContext(ServerChatOS server, SelectionKey key){
        this.key = key;
        this.sc = (SocketChannel) key.channel();
        this.server = server;
        this.serverFrameVisitor = new ServerFrameVisitor(server, this);
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public SelectionKey getKey() {
        return key;
    }

    /**
     * Process the content of bbin
     * The convention is that bbin is in write-mode before the call
     * to process and after the call
     */
    private void processIn() {
        var result = frameReader.process(bbin);
        switch (result){
            case DONE -> {
                var frame = frameReader.get();
                frameReader.reset();
                treatFrame(frame);
            }
            case REFILL -> {}
            case ERROR -> logger.warning("Corrupted message received");
        }
    }

    private void treatFrame(Frame frame) {
        frame.accept(serverFrameVisitor);
    }

    /**
     * Add a message to the message queue, tries to fill bbOut and updateInterestOps
     *
     * @param bb ByteBuffer to send
     */
    public void queueMessage(ByteBuffer bb) {
        queue.add(bb);
        processOut();
        updateInterestOps();
    }

    /**
     * Try to fill bbout from the message queue
     */
    private void processOut() {
        while (!queue.isEmpty()){
            var bb = queue.peek();
            if ( bb.remaining() <= bbout.remaining() ){
                queue.remove();
                bbout.put(bb);
            } else {
                break;
            }
        }
    }

    /**
     * Update the interestOps of the key looking
     * only at values of the boolean closed and
     * of both ByteBuffers.
     *
     * The convention is that both buffers are in write-mode before the call
     * to updateInterestOps and after the call.
     * Also it is assumed that process has been be called just
     * before updateInterestOps.
     */
    private void updateInterestOps() {
        var intOps = 0;
        if( !closed && bbin.hasRemaining() ){ //
            intOps |= SelectionKey.OP_READ;
        }
        if( bbout.position() != 0  ) { // Hmmm
            intOps |= SelectionKey.OP_WRITE;
        }
        if( intOps == 0 ){
            logger.info("Closing client");
            silentlyClose();
            return;
        }
        key.interestOps(intOps);
    }

    /**
     * Close a SocketChannel
     */
    public void silentlyClose() {
        try {
            sc.close();
            server.removeClientFromMap(login);
        } catch (IOException e) {
            // ignore exception
        }
    }

    /**
     * Performs the read action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doRead and after the call
     *
     * @throws IOException
     */
    public void doRead() throws IOException {
        if( sc.read(bbin) == -1 ){
            logger.info(" Client has closed its connection ");
            closed = true;
            silentlyClose();
            return;
        }
        processIn();
        updateInterestOps();
    }

    /**
     * Performs the write action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doWrite and after the call
     *
     * @throws IOException
     */
    public void doWrite() throws IOException {
        bbout.flip();
        sc.write(bbout);
        bbout.compact();
        processOut();
        updateInterestOps();
    }
}
