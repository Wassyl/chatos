package fr.umlv.chatos.context.client;

import fr.umlv.chatos.client.ClientChatOS;
import fr.umlv.chatos.packet.*;
import fr.umlv.chatos.reader.FrameReader;
import fr.umlv.chatos.visitor.ClientFrameVisitor;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Logger;

/**
 * Creating a client context in a public connection
 */
public class ClientPublicContext implements ClientContext {

    /**
     * Logger to announce info, warning or error
     */
    private static final Logger logger = Logger.getLogger(ClientPublicContext.class.getName());

    /**
     * The SelectionKey
     */
    private final SelectionKey key;

    /**
     * The socketChannel
     */
    private final SocketChannel sc;

    /**
     * Bytebuffer of the reader
     */
    private final ByteBuffer bbin = ByteBuffer.allocateDirect(BUFFER_SIZE);

    /**
     * Bytebuffer of the sender
     */
    private final ByteBuffer bbout = ByteBuffer.allocateDirect(BUFFER_SIZE);

    /**
     * A queue of messages in Bytebuffer form
     */
    private final Queue<ByteBuffer> queue = new LinkedList<>(); // buffers read-mode

    /**
     * A commandReader to read the command and according to the command we will do the right thing
     */
    private final FrameReader frameReader = new FrameReader();

    private final ClientFrameVisitor clientFrameVisitor;

    /**
     * Create a new client
     */
    private final ClientChatOS client;

    private boolean closed = false;

    /**
     * Create a new Client Context in the public connection
     * @param key Key generated with connection
     * @param client ClientChatOS instance
     */
    public ClientPublicContext(SelectionKey key, ClientChatOS client){
        this.key = key;
        this.sc = (SocketChannel) key.channel();
        this.client = client;
        this.clientFrameVisitor = new ClientFrameVisitor(client, this);
    }

    /**
     * Process the content of bbin
     */
    public void processIn() {
        // Recover next Command sent by the server
        switch ( frameReader.process(bbin) ){
            case DONE -> {
                var frame = frameReader.get();
                frameReader.reset();
                treatFrame(frame);
            }
            case REFILL-> {}
            case ERROR -> logger.severe("Corrupted message received");
        }
    }

    private void treatFrame(Frame frame) {
        frame.accept(clientFrameVisitor);
    }

    /**
     * Add a message to the message queue, tries to fill bbOut and updateInterestOps
     *
     * @param bb ByteBuffer to enqueue
     */
    public void queueMessage(ByteBuffer bb) {
        queue.add(bb);
        processOut();
        updateInterestOps();
    }

    /**
     * Try to fill bbout from the message queue
     */
    private void processOut() {
        while (!queue.isEmpty()){
            var bb = queue.peek();
            if ( bb.remaining() <= bbout.remaining() ){
                queue.remove();
                bbout.put(bb);
            } else {
                break;
            }
        }
    }

    /**
     * Update the interestOps of the key looking
     * only at values of the boolean closed and
     * of both ByteBuffers.
     *
     * The convention is that both buffers are in write-mode before the call
     * to updateInterestOps and after the call.
     * Also it is assumed that process has been be called just
     * before updateInterestOps.
     */
    private void updateInterestOps() {
        var interesOps=0;
        if (!closed && bbin.hasRemaining()){
            interesOps |= SelectionKey.OP_READ;
        }
        if ( bbout.position() != 0 ){
            interesOps |= SelectionKey.OP_WRITE;
        }
        if (interesOps == 0){
            client.close();
            return;
        }
        key.interestOps(interesOps);
    }

    /**
     * Performs the read action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doRead and after the call
     *
     * @throws IOException with read()
     */
    public void doRead() throws IOException {
        if (sc.read(bbin)==-1) {
            logger.severe("Server has closed its connection");
            silentlyClose();
            return;
        }
        processIn();
        updateInterestOps();
    }

    /**
     * Performs the write action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doWrite and after the call
     *
     * @throws IOException From write()
     */
    public void doWrite() throws IOException {
        bbout.flip();
        sc.write(bbout);
        bbout.compact();
        processOut();
        updateInterestOps();
    }

    /**
     * Connection to the server
     * @throws IOException From finishConnect()
     */
    public void doConnect() throws IOException {
        if( !sc.finishConnect() ) {
            logger.info("Still trying to connect to host");
            return;
        }
        logger.info("Server reached");
        queueMessage( new Login(client.getLogin()).asByteBuffer() );
        updateInterestOps();
    }

    public void silentlyClose() {
        closed = true;
        client.close();
    }
}