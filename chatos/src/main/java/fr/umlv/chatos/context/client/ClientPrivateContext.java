package fr.umlv.chatos.context.client;

import fr.umlv.chatos.client.ClientChatOS;
import fr.umlv.chatos.packet.Frame;
import fr.umlv.chatos.packet.LoginPrivate;
import fr.umlv.chatos.reader.PrivatePacketReader;
import fr.umlv.chatos.reader.FrameReader;
import fr.umlv.chatos.visitor.ClientFramePrivateVisitor;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channel;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Logger;

/**
 * Creating a client context in a private connection
 */
public class ClientPrivateContext implements ClientContext {

    /**
     * Logger to announce info, warning or error
     */
    private static final Logger logger = Logger.getLogger(ClientPrivateContext.class.getName());

    /**
     * The SelectionKey
     */
    private final SelectionKey key;

    /**
     * The SocketChannel
     */
    private final SocketChannel sc;

    /**
     * Bytebuffer of the reader
     */
    private final ByteBuffer bbin = ByteBuffer.allocateDirect(BUFFER_SIZE);

    /**
     * ByteBuffer of the sender
     */
    private final ByteBuffer bbout = ByteBuffer.allocateDirect(BUFFER_SIZE);

    /**
     * A queue of messages in Bytebuffer form
     */
    private final Queue<ByteBuffer> queue = new LinkedList<>(); // buffers read-mode

    /**
     * A commandReader to read the command and according to the command we will do the right thing
     */
    private final FrameReader frameReader = new FrameReader();

    /**
     * A ByteReader to read the command and according to the command we will do the right thing
     */
    private final PrivatePacketReader privatePacketReader = new PrivatePacketReader();

    private final ClientFramePrivateVisitor clientFramePrivateVisitor;
    /**
     * Create a new client
     */
    private final ClientChatOS client;

    /**
     * A long to identify the connection between two clients
     */
    private final long connectId;

    /**
     * String of the otherClient
     */
    private final String otherClient;

    private boolean closed = false;

    private enum State { NOT_LOGGED, LOGGED };

    private State state;
    /**
     * Create a new Client Context in the private connection
     * @param key
     * @param client
     * @param connectId
     */
    public ClientPrivateContext(SelectionKey key, ClientChatOS client, String otherClient, long connectId){
        this.key = key;
        this.sc = (SocketChannel) key.channel();
        this.client = client;
        this.connectId = connectId;
        this.otherClient = otherClient;
        this.clientFramePrivateVisitor = new ClientFramePrivateVisitor(client, this);
        state = State.NOT_LOGGED;
    }

    /**
     * Process the content of bbin
     */
    private void processIn() {
        switch ( state ) {
            case LOGGED -> doLogged();
            case NOT_LOGGED -> doNotLogged();
        }
    }

    private void doNotLogged() {
        switch (frameReader.process(bbin)){
            case DONE -> {
                var frame = frameReader.get();
                frameReader.reset();
                treatFrame(frame);
            }
            case REFILL -> {}
            case ERROR -> logger.warning("Corrupted private connection message received");
        }
    }

    private void doLogged() {
        switch (privatePacketReader.process(bbin)){
            case DONE -> {
                var frame = privatePacketReader.get();
                privatePacketReader.reset();
                treatFrame(frame);
            }
            case REFILL -> {}
            case ERROR -> logger.warning("Corrupted private connection message received");
        }
    }

    public long getConnectId() {
        return connectId;
    }
    public void setLogged() {
        state = State.LOGGED;
    }

    private void treatFrame(Frame frame){
        frame.accept(clientFramePrivateVisitor);
    }

    /**
     * Add a message to the message queue, tries to fill bbOut and updateInterestOps
     *
     * @param bb ByteBuffer to send
     */
    public void queueMessage(ByteBuffer bb) {
        queue.add(bb);
        processOut();
        updateInterestOps();
    }

    /**
     * Try to fill bbout from the message queue
     */
    private void processOut() {
        while (!queue.isEmpty()){
            var bb = queue.peek();
            if ( bb.remaining() <= bbout.remaining() ){
                queue.remove();
                bbout.put(bb);
            } else {
                break;
            }
        }
    }

    /**
     * Update the interestOps of the key looking
     * only at values of the boolean closed and
     * of both ByteBuffers.
     *
     * The convention is that both buffers are in write-mode before the call
     * to updateInterestOps and after the call.
     * Also it is assumed that process has been be called just
     * before updateInterestOps.
     */
    private void updateInterestOps() {
        var interesOps=0;
        if (!closed && bbin.hasRemaining()){
            interesOps |= SelectionKey.OP_READ;
        }
        if ( bbout.position() != 0 ){
            interesOps |= SelectionKey.OP_WRITE;
        }
        if (interesOps == 0){
            client.close();
            return;
        }
        key.interestOps(interesOps);
    }

    /**
     * Performs the read action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doRead and after the call
     *
     * @throws IOException From read()
     */
    public void doRead() throws IOException {
        if (sc.read(bbin)==-1) {
            logger.severe("Server has closed the private connection");
            closed=true;
            close();
            return;
        }
        processIn();
        updateInterestOps();
    }

    /**
     * Performs the write action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doWrite and after the call
     *
     * @throws IOException from write()
     */
    public void doWrite() throws IOException {
        bbout.flip();
        sc.write(bbout);
        bbout.compact();
        processOut();
        updateInterestOps();
    }

    /**
     * Connection to the server
     * @throws IOException
     */
    public void doConnect() throws IOException {
        if( !sc.finishConnect() ) {
            logger.info("Still trying to connect to host");
            return;
        }
        logger.info("Private connection initiated, logging in ...");
        queueMessage( new LoginPrivate(connectId).asByteBuffer() );
        updateInterestOps();
    }

    /**
     * Close a SocketChannel
     */
    public void silentlyClose() {
        var sc = (Channel) key.channel();
        try {
            sc.close();
            logger.info("Closing private connection with id :" + connectId);
        } catch (IOException e) {
            // ignore exception
        }
    }

    /**
     * Only called when the other client has closed his connection
     */
    public void close() {
        logger.info("Closing private connection");
        silentlyClose();
        client.closePrivateConnectionFor(otherClient);
    }
}