package fr.umlv.chatos.visitor;

import fr.umlv.chatos.client.ClientChatOS;
import fr.umlv.chatos.context.client.ClientContext;
import fr.umlv.chatos.context.client.ClientPublicContext;
import fr.umlv.chatos.packet.*;
import java.util.logging.Logger;

public class ClientFrameVisitor implements FrameVisitor {

    private static final Logger logger = Logger.getLogger(ClientFrameVisitor.class.getName());
    private final ClientContext ctx;
    private final ClientChatOS client;

    public ClientFrameVisitor( ClientChatOS client, ClientContext ctx){
        this.ctx = ctx;
        this.client = client;
    }

    @Override
    public void visit(Login login) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(LoginAccepted loginAccepted) {
        logger.info("Successfully joined the server ");
    }

    @Override
    public void visit(LoginRefused loginRefused) {
        logger.severe("Failed to log to the server");
        client.close();
    }

    @Override
    public void visit(Message message) {
        System.out.println(message); // Check toString() methods for each Command
    }

    @Override
    public void visit(MessagePrivate messagePrivate) {
        System.out.println(messagePrivate); // Check toString() methods for each Command
    }

    @Override
    public void visit(RequestPrivate requestPrivate) {
        client.addRequester( requestPrivate.getRequester() );
        System.out.println(requestPrivate); // Print the request the client just received
    }

    @Override
    public void visit(OkPrivate okPrivate) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(KoPrivate koPrivate) {
        System.out.println(koPrivate); // Check toString() methods for each Command
    }

    @Override
    public void visit(IdPrivate idPrivate) {
        System.out.println(idPrivate);
        var other = idPrivate.getRequester().equals(client.getLogin()) ? idPrivate.getTarget() : idPrivate.getRequester();
        client.addPrivateConnection( other, idPrivate.getConnectId() );
    }

    @Override
    public void visit(LoginPrivate loginPrivate) {
        logger.severe("Received LoginPrivate - " + loginPrivate);
        logger.severe("Closing client");
        ctx.silentlyClose();
    }

    @Override
    public void visit(Established established) {
        logger.info("<3 Private connection successfully initiated <3 !");
    }

    @Override
    public void visit(PrivatePacket packet) {
        System.out.println(packet);
    }
}
