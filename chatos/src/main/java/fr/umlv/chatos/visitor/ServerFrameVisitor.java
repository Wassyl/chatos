package fr.umlv.chatos.visitor;

import fr.umlv.chatos.context.server.ServerContext;
import fr.umlv.chatos.context.server.ServerPublicContext;
import fr.umlv.chatos.packet.*;
import fr.umlv.chatos.server.ServerChatOS;

import java.util.logging.Logger;

public class ServerFrameVisitor implements FrameVisitor {

    private static final Logger logger = Logger.getLogger(ServerFrameVisitor.class.getName());
    private final ServerPublicContext ctx;
    private final ServerChatOS server;

    public ServerFrameVisitor(ServerChatOS server, ServerPublicContext ctx){
        this.ctx = ctx;
        this.server = server;
    }

    @Override
    public void visit(Login login) {
        server.addingNewClient( login.getRequester(), ctx);
    }

    @Override
    public void visit(LoginAccepted loginAccepted) {
        logger.warning("Received LoginAccepted, closing client");
        ctx.silentlyClose();
    }

    @Override
    public void visit(LoginRefused loginRefused) {
        logger.warning("Received loginRefused, closing client");
        ctx.silentlyClose();
    }

    @Override
    public void visit(Message message) {
        server.broadcast(message);
    }

    @Override
    public void visit(MessagePrivate messagePrivate) {
        server.privateMessageTo(messagePrivate);
    }

    @Override
    public void visit(RequestPrivate requestPrivate) {
        server.requestPrivateTo( requestPrivate );
    }

    @Override
    public void visit(OkPrivate okPrivate) {
        server.confirmPrivateConnection( okPrivate.getRequester(), okPrivate.getTarget() );
    }

    @Override
    public void visit(KoPrivate koPrivate) {
        server.koPrivateTo( koPrivate );
    }

    @Override
    public void visit(IdPrivate idPrivate) {
        logger.warning("Received IdPrivate, closing client");
        ctx.silentlyClose();
    }

    @Override
    public void visit(LoginPrivate loginPrivate) {
        server.handleLoginPrivate(loginPrivate.getConnectId(), ctx);
    }

    @Override
    public void visit(Established established) {
        logger.warning("Received Established, closing client");
        ctx.silentlyClose();
    }

    @Override
    public void visit( PrivatePacket packet ) {
        logger.warning("Received PrivatePacket, closing client");
        ctx.silentlyClose();
    }
}
