package fr.umlv.chatos.visitor;

import fr.umlv.chatos.context.server.ServerPrivateContext;
import fr.umlv.chatos.packet.*;
import fr.umlv.chatos.server.ServerChatOS;

import java.util.logging.Logger;

public class ServerFramePrivateVisitor implements FrameVisitor {

    private static final Logger logger = Logger.getLogger(ServerFramePrivateVisitor.class.getName());
    private final ServerPrivateContext ctx;
    private final ServerChatOS server;

    public ServerFramePrivateVisitor(ServerChatOS server, ServerPrivateContext ctx){
        this.ctx = ctx;
        this.server = server;
    }

    @Override
    public void visit(Login login) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(LoginAccepted loginAccepted) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(LoginRefused loginRefused) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(Message message) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(MessagePrivate messagePrivate) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(RequestPrivate requestPrivate) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(OkPrivate okPrivate) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(KoPrivate koPrivate) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(IdPrivate idPrivate) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(LoginPrivate loginPrivate) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(Established established) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(PrivatePacket privatePacket) {
        server.sendPrivatePacketFrom(ctx, privatePacket);
    }
}
