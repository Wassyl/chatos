package fr.umlv.chatos.visitor;

import fr.umlv.chatos.client.ClientChatOS;
import fr.umlv.chatos.context.client.ClientPrivateContext;
import fr.umlv.chatos.packet.*;

import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

public class ClientFramePrivateVisitor implements FrameVisitor {

    private static final Logger logger = Logger.getLogger(ClientFramePrivateVisitor.class.getName());
    private final ClientPrivateContext ctx;
    private final ClientChatOS client;

    public ClientFramePrivateVisitor( ClientChatOS client, ClientPrivateContext ctx){
        this.ctx = ctx;
        this.client = client;
    }

    @Override
    public void visit(Login login) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(LoginAccepted loginAccepted) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(LoginRefused loginRefused) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(Message message) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(MessagePrivate messagePrivate) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(RequestPrivate requestPrivate) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(OkPrivate okPrivate) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(KoPrivate koPrivate) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(IdPrivate idPrivate) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(LoginPrivate loginPrivate) {
        ctx.silentlyClose();
    }

    @Override
    public void visit(Established established) {
        logger.info("<3 Private connection successfully initiated <3 !");
        ctx.setLogged();
    }

    @Override
    public void visit(PrivatePacket packet) {
        var bb = packet.asByteBuffer();
        System.out.println("Received : " + bb.remaining() + " bytes (from : " + ctx.getConnectId() +")");
    }
}
