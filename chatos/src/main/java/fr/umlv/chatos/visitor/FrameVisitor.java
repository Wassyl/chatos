package fr.umlv.chatos.visitor;

import fr.umlv.chatos.packet.*;

public interface FrameVisitor {

    void visit(Login login);

    void visit(LoginAccepted loginAccepted);

    void visit(LoginRefused loginRefused);

    void visit(Message message);

    void visit(MessagePrivate messagePrivate);

    void visit(RequestPrivate requestPrivate);

    void visit(OkPrivate okPrivate);

    void visit(KoPrivate koPrivate);

    void visit(IdPrivate idPrivate);

    void visit(LoginPrivate loginPrivate);

    void visit(Established established);

    void visit(PrivatePacket packet);
}
