package fr.umlv.chatos.reader;

import fr.umlv.chatos.packet.MessagePrivate;

import java.nio.ByteBuffer;

/**
 * A MessagePrivateReader to read a message in private connection
 */
public class MessagePrivateReader implements Reader<MessagePrivate>  {

    /**
     * Enumeration of state
     */
    private enum State {DONE, NEXT, HAS_REQUESTER_TARGET};

    /**
     * Define state of the beginning
     */
    private State state = State.NEXT;

    /**
     * A Request Target Reader to read the login, the target and the message private
     */
    private final RequesterTargetReader requesterTargetReader = new RequesterTargetReader();

    /**
     * A String Reader to read the login, the target and the message private
     */
    private final StringReader stringReader = new StringReader();

    /**
     * The message private
     */
    private MessagePrivate messagePrivate;

    /**
     * The requester target
     */
    private RequesterTarget requesterTarget;

    /**
     * Process the bytebuffer according to the state
     * Create a Message Private by reading the sender, the target and then the message corresponding.
     * @param bb
     * @return the processus Status
     */
    @Override
    public ProcessStatus process(ByteBuffer bb) {
        switch (state){
            case NEXT:
                var resultRequester = requesterTargetReader.process(bb);
                if( resultRequester != ProcessStatus.DONE ) {
                    return resultRequester;
                }
                requesterTarget = requesterTargetReader.get();
                requesterTargetReader.reset();
                state = State.HAS_REQUESTER_TARGET;
            case HAS_REQUESTER_TARGET:
                var resultMessage = stringReader.process(bb);
                if( resultMessage != ProcessStatus.DONE ) {
                    return resultMessage;
                }
                var message = stringReader.get();
                messagePrivate = new MessagePrivate(requesterTarget.getRequester(), requesterTarget.getTarget(), message);
                state = State.DONE;
                return ProcessStatus.DONE;
            default:
                throw new IllegalStateException();
        }
    }

    /**
     * Get the message private
     * @return the message private
     */
    @Override
    public MessagePrivate get() {
        if ( state != state.DONE ) {
            throw new IllegalStateException();
        }
        return messagePrivate;
    }

    /**
     * Reset the process
     */
    @Override
    public void reset() {
        state = State.NEXT;
        stringReader.reset();
        requesterTargetReader.reset();
    }
}
