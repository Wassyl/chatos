package fr.umlv.chatos.reader;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class StringReader implements Reader<String> {

    /**
     * Enumeration of state
     */
    private enum State {DONE, NEXT, GOT_SIZE, ERROR};

    /**
     * Define state of the beginning
     */
    private State state = State.NEXT;

    /**
     * CHARSET UTF8
     */
    private final Charset UTF_8 = StandardCharsets.UTF_8;

    /**
     * Initialize the bytebuffer of internalbb
     */
    private final ByteBuffer internalbb = ByteBuffer.allocate(1024); // write-mode

    /**
     * The message
     */
    private String value;

    /**
     * IntReader to read the size of the message
     */
    private final IntReader intReader = new IntReader();

    /**
     * Process the bytebuffer according to the state to read the size of the message then the message in UTF8
     * @param bb
     * @return the processus Status
     */
    @Override
    public ProcessStatus process(ByteBuffer bb) {
        switch (state){
            case NEXT:
                var result = intReader.process(bb);
                if( result != ProcessStatus.DONE ) {
                    return result; // renvoie soit REFILL soit ERROR
                }
                var sizeData = intReader.get();
                if( sizeData < 0 || sizeData > 1024 ) {
                    state = State.ERROR;
                    return ProcessStatus.ERROR;
                }
                state = State.GOT_SIZE;
                internalbb.limit(sizeData);
            case GOT_SIZE:
                bb.flip();
                try {
                    if (bb.remaining() <= internalbb.remaining() ){
                        internalbb.put(bb);
                    } else {
                        var oldLimit = bb.limit();
                        bb.limit(internalbb.remaining());
                        internalbb.put(bb);
                        bb.limit(oldLimit);
                    }
                } finally {
                    bb.compact();
                }
                // Read string
                if( internalbb.hasRemaining() ) {
                    return ProcessStatus.REFILL;
                }
                internalbb.flip();
                value = UTF_8.decode(internalbb).toString();
                state = State.DONE;
                return ProcessStatus.DONE;
            default:
                throw new IllegalStateException();
        }
    }

    /**
     * Get the string value
     * @return the string value
     */
    @Override
    public String get() {
        if ( state != state.DONE) {
            throw new IllegalStateException();
        }
        return value;
    }

    /**
     * Reset the process
     */
    @Override
    public void reset() {
        state = State.NEXT;
        internalbb.clear();
        intReader.reset();
    }
}
