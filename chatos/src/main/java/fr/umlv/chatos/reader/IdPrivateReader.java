package fr.umlv.chatos.reader;

import fr.umlv.chatos.packet.IdPrivate;

import java.nio.ByteBuffer;

/**
 * A IdPrivateReader to read twice a string
 */
public class IdPrivateReader implements Reader<IdPrivate> {

    /**
     * Enumeration of state
     */
    private enum State {DONE, NEXT, HAS_REQUESTER_TARGET};

    /**
     * Define state of the beginning
     */
    private State state = State.NEXT;

    /**
     * The Object Idprivate with the sender, target and the connectId
     */
    private IdPrivate idPrivate;

    /**
     * A RequesterTarget for the two logins (sender, target)
     */
    private RequesterTarget requesterTarget;

    /**
     * A Long Reader to read the Connect id (Long)
     */
    private final LongReader longReader = new LongReader();

    /**
     * A requesterTargetReader to read the string of the requester and the target
     */
    private final RequesterTargetReader requesterTargetReader = new RequesterTargetReader();

    /**
     * Process the bytebuffer according to the state to read the idPrivate value
     * @param bb
     * @return the processus Status
     */
    @Override
    public ProcessStatus process(ByteBuffer bb) {
        switch (state){
            case NEXT:
                var result = requesterTargetReader.process(bb);
                if( result != ProcessStatus.DONE ) {
                    return result;
                }
                requesterTarget = requesterTargetReader.get();
                requesterTargetReader.reset();
                state = State.HAS_REQUESTER_TARGET;
            case HAS_REQUESTER_TARGET:
                var resultLong = longReader.process(bb);
                if( resultLong != ProcessStatus.DONE ) {
                    return resultLong;
                }
                var id = longReader.get();
                idPrivate = new IdPrivate(requesterTarget.getRequester(), requesterTarget.getTarget(), id);
                state = State.DONE;
                return ProcessStatus.DONE;
            default:
                throw new IllegalStateException();
        }
    }

    /**
     * Get the value
     * @return the value
     */
    @Override
    public IdPrivate get() {
        if ( state != state.DONE ) {
            throw new IllegalStateException();
        }
        return idPrivate;
    }

    /**
     * Reset the process
     */
    @Override
    public void reset() {
        state = State.NEXT;
        requesterTargetReader.reset();
        longReader.reset();
    }

}
