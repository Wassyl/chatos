package fr.umlv.chatos.reader;

/**
 * The class to create a Requester and a target link together
 */
public class RequesterTarget {

    /**
     * The requester
     */
    private final String requester;

    /**
     * The target
     */
    private final String target;

    /**
     * Initialize the requester and the target
     * @param requester
     * @param target
     */
    RequesterTarget(String requester, String target){
        this.requester = requester;
        this.target = target;
    }

    /**
     * Get the requester
     * @return the requester
     */
    public String getRequester(){
        return requester;
    }

    /**
     * Get the target
     * @return the target
     */
    public String getTarget(){
        return target;
    }
}
