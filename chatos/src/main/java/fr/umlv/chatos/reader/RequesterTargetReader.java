package fr.umlv.chatos.reader;

import java.nio.ByteBuffer;

/**
 * A RequesterTarget to read the requester and the target
 */
public class RequesterTargetReader implements Reader<RequesterTarget>{

    /**
     * Enumeration of state
     */
    private enum State {DONE, NEXT, HAS_REQUESTER};

    /**
     * Define state of the beginning
     */
    private State state = State.NEXT;

    /**
     * A String Reader to read the login, the target and the message private
     */
    private final StringReader stringReader = new StringReader();

    /**
     * The object Requester and Target
     */
    private RequesterTarget requesterTarget;

    /**
     * The name of the requester
     */
    private String requester;

    /**
     * Process the bytebuffer according to the state
     * Create a new RequesterTarget by getting the requester and the target
     * @param bb
     * @return the processus Status
     */
    @Override
    public ProcessStatus process(ByteBuffer bb) {
        switch (state){
            case NEXT:
                var resultRequester = stringReader.process(bb);
                if( resultRequester != ProcessStatus.DONE ) {
                    return resultRequester;
                }
                requester = stringReader.get();
                stringReader.reset();
                state = State.HAS_REQUESTER;
            case HAS_REQUESTER:
                var resultTarget = stringReader.process(bb);
                if( resultTarget != ProcessStatus.DONE ) {
                    return resultTarget;
                }
                var target = stringReader.get();
                requesterTarget = new RequesterTarget(requester, target);
                state = State.DONE;
                return ProcessStatus.DONE;
            default:
                throw new IllegalStateException();
        }
    }

    /**
     * Get the requester Target
     * @return the requester Target
     */
    @Override
    public RequesterTarget get() {
        if ( state != state.DONE ) {
            throw new IllegalStateException();
        }
        return requesterTarget;
    }

    /**
     * Reset the process
     */
    @Override
    public void reset() {
        state = State.NEXT;
        stringReader.reset();
    }

}
