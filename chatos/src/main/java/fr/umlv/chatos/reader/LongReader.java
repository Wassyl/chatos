package fr.umlv.chatos.reader;

import java.nio.ByteBuffer;

/**
 * A LongReader to read an long value
 */
public class LongReader implements Reader<Long> {

    /**
     * Enumeration of state
     */
    private enum State {DONE,WAITING,ERROR};

    /**
     * Define state of the beginning
     */
    private State state = State.WAITING;

    /**
     * Initialize the bytebuffer of internalbb
     */
    private final ByteBuffer internalbb = ByteBuffer.allocate(Long.BYTES); // write-mode

    /**
     * The value we want to read
     */
    private long value;

    /**
     * Process the bytebuffer according to the state to read the long value
     * @param bb
     * @return the processus Status
     */
    @Override
    public ProcessStatus process(ByteBuffer bb) {
        if (state== State.DONE || state== State.ERROR) {
            throw new IllegalStateException();
        }
        bb.flip();
        try {
            if (bb.remaining()<=internalbb.remaining()){
                internalbb.put(bb);
            } else {
                var oldLimit = bb.limit();
                bb.limit(internalbb.remaining());
                internalbb.put(bb);
                bb.limit(oldLimit);
            }
        } finally {
            bb.compact();
        }
        if (internalbb.hasRemaining()){
            return ProcessStatus.REFILL;
        }
        state=State.DONE;
        internalbb.flip();
        value = internalbb.getLong();
        return ProcessStatus.DONE;
    }

    /**
     * Get the value
     * @return the value
     */
    @Override
    public Long get() {
        if (state!= State.DONE) {
            throw new IllegalStateException();
        }
        return value;
    }

    /**
     * Reset the process
     */
    @Override
    public void reset() {
        state= State.WAITING;
        internalbb.clear();
    }
}
