package fr.umlv.chatos.reader;

import fr.umlv.chatos.packet.Message;

import java.nio.ByteBuffer;

public class MessageReader implements Reader<Message>  {
    /**
     * Enumeration of state
     */
    private enum State {DONE, NEXT, HAS_LOGIN};

    /**
     * Define state of the beginning
     */
    private State state = State.NEXT;

    /**
     * The Object message
     */
    private Message message;

    /**
     * The login
     */
    private String login;

    /**
     * The String message
     */
    private String text;

    /**
     * The String Reader to read the message and the login
     */
    private final StringReader stringReader = new StringReader();

    /**
     * Process the bytebuffer according to the state
     * Create a new message object by reading the login and the message (String) in public connection
     * @param bb
     * @return the processus Status
     */
    @Override
    public ProcessStatus process(ByteBuffer bb) {
        switch (state){
            case NEXT:
                var result = stringReader.process(bb);
                if( result != ProcessStatus.DONE ) {
                    return result;
                }
                login = stringReader.get();
                stringReader.reset();
                state = State.HAS_LOGIN;
            case HAS_LOGIN:
                var resultLogin = stringReader.process(bb);
                if( resultLogin != ProcessStatus.DONE ) {
                    return resultLogin;
                }
                text = stringReader.get();
                message = new Message(login, text);
                state = State.DONE;
                return ProcessStatus.DONE;
            default:
                throw new IllegalStateException();
        }
    }

    /**
     * Get the message
     * @return the message
     */
    @Override
    public Message get() {
        if ( state != state.DONE ) {
            throw new IllegalStateException();
        }
        return message;
    }

    /**
     * Reset the process
     */
    @Override
    public void reset() {
        state = State.NEXT;
        stringReader.reset();
    }
}
