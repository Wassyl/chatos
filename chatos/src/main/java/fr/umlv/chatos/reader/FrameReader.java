package fr.umlv.chatos.reader;

import fr.umlv.chatos.packet.*;
import fr.umlv.chatos.packet.Message;

import java.nio.ByteBuffer;

/**
 * Class Command Reader : Read command
 */
public class FrameReader implements Reader<Frame> {

    /**
     * Enumeration of state
     */
    private enum State {DONE, NEXT, HAS_OPERAND};

    /**
     * Define state of the beginning
     */
    private State state = State.NEXT;

    /**
     * The command
     */
    private Frame frame;

    /**
     * The opcode of the command
     */
    private Frame.Operands opcode;

    /**
     * A LongReader to read a long
     */
    private final LongReader longReader = new LongReader();

    /**
     * A StringReader to read a string with its size
     */
    private final StringReader stringReader = new StringReader();

    /**
     * A MessageReader to read a message with the login adn the message
     */
    private final MessageReader messageReader = new MessageReader();

    /**
     * A MessagePrivateReader to read a message private with the sender, target and message
     */
    private final MessagePrivateReader messagePrivateReader = new MessagePrivateReader();

    /**
     * A requesterTargetReader to read the string of the requester and the target
     */
    private final RequesterTargetReader requesterTargetReader = new RequesterTargetReader();

    /**
     * An IdPrivateReader to read the connect id of the private connection with the sender and the target
     */
    private final IdPrivateReader idPrivateReader = new IdPrivateReader();

    /**
     * Process the bytebuffer according to the state and the Command opcode
     * @param bb Reception byteBuffer
     * @return the processus Status
     */
    @Override
    public ProcessStatus process(ByteBuffer bb) {
        switch (state){
            case NEXT:
                if(bb.position() == 0 ){
                    return ProcessStatus.REFILL;
                }
                bb.flip();
                opcode = Frame.getOperandsFor(bb.get()); // Throws IllegalArgumentException if unrecognized
                bb.compact();
                state = State.HAS_OPERAND;
            case HAS_OPERAND :
                switch (opcode){
                    case LOGIN -> {
                        var result = stringReader.process(bb);
                        if( result != ProcessStatus.DONE ) {
                            return result;
                        }
                        var string = stringReader.get();
                        frame = new Login(string);
                        state = State.DONE;
                    }
                    case LOGIN_ACCEPTED -> frame = new LoginAccepted();
                    case LOGIN_REFUSED -> frame = new LoginRefused();
                    case MESSAGE -> {
                        var result = messageReader.process(bb);
                        if( result != ProcessStatus.DONE ) {
                            return result;
                        }
                        var message = messageReader.get();
                        frame = new Message(message.getRequester(), message.getText());
                    }
                    case MESSAGE_PRIVATE -> {
                        var result = messagePrivateReader.process(bb);
                        if( result != ProcessStatus.DONE ) {
                            return result;
                        }
                        var message = messagePrivateReader.get();
                        frame = new MessagePrivate(message.getRequester(), message.getTarget(), message.getText());
                    }
                    case REQUEST_PRIVATE -> {
                        var result = requesterTargetReader.process(bb);
                        if( result != ProcessStatus.DONE ) {
                            return result;
                        }
                        var message = requesterTargetReader.get();
                        frame = new RequestPrivate(message.getRequester(), message.getTarget());
                    }
                    case OK_PRIVATE -> {
                        var result = requesterTargetReader.process(bb);
                        if( result != ProcessStatus.DONE ) {
                            return result;
                        }
                        var message = requesterTargetReader.get();
                        frame = new OkPrivate(message.getRequester(), message.getTarget());
                    }
                    case KO_PRIVATE -> {
                        var result = requesterTargetReader.process(bb);
                        if( result != ProcessStatus.DONE ) {
                            return result;
                        }
                        var message = requesterTargetReader.get();
                        frame = new KoPrivate(message.getRequester(), message.getTarget());
                    }
                    case ID_PRIVATE -> {
                        var result = idPrivateReader.process(bb);
                        if( result != ProcessStatus.DONE ) {
                            return result;
                        }
                        var idPrivate = idPrivateReader.get();
                        frame = new IdPrivate(idPrivate.getRequester(), idPrivate.getTarget(), idPrivate.getConnectId());
                    }
                    case LOGIN_PRIVATE -> {
                        var result = longReader.process(bb);
                        if( result != ProcessStatus.DONE ) {
                            return result;
                        }
                        frame = new LoginPrivate( longReader.get() );
                    }
                    case ESTABLISHED -> frame = new Established();
                    default -> throw new IllegalStateException("Unknown operand found when reading command : " + opcode);
                }
                state = State.DONE;
                return ProcessStatus.DONE;
            default : throw new IllegalStateException("Unknown operand found when reading command : " + opcode);
        }
    }

    /**
     * Get the frame
     * @return frame read
     */
    @Override
    public Frame get() {
        if ( state != State.DONE) {
            throw new IllegalStateException();
        }
        return frame;
    }

    /**
     * Reset the process
     */
    @Override
    public void reset() {
        longReader.reset();
        stringReader.reset();
        messageReader.reset();
        messagePrivateReader.reset();

        requesterTargetReader.reset();
        idPrivateReader.reset();

        state = State.NEXT;
    }
}