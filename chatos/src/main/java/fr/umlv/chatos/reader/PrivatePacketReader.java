package fr.umlv.chatos.reader;

import fr.umlv.chatos.packet.PrivatePacket;

import java.nio.ByteBuffer;

/**
 * A class to read a private message which are just bytes with a specific
 */
public class PrivatePacketReader implements Reader<PrivatePacket> {

    /**
     * Enumeration of state
     */
    private enum State {DONE, NEXT, GOT_SIZE, ERROR};

    /**
     * Define state of the beginning
     */
    private State state = State.NEXT;

    /**
     * Initialize the bytebuffer of internalbb
     */
    private final ByteBuffer internalbb = ByteBuffer.allocate(1024); // write-mode

    /**
     * The message
     */
    private ByteBuffer buffer;

    private PrivatePacket privatePacket;
    /**
     * IntReader to read the size of the message
     */
    private final IntReader intReader = new IntReader();

    /**
     * Process the bytebuffer according to the state to read the size of the message then the message in UTF8
     * @param bb
     * @return the processus Status
     */
    @Override
    public ProcessStatus process(ByteBuffer bb) {
        switch (state){
            case NEXT:
                var result = intReader.process(bb);
                if( result != ProcessStatus.DONE ) {
                    return result; // renvoie soit REFILL soit ERROR
                }
                var sizeData = intReader.get();
                if( sizeData < 0 ) { // il a écrit de la merde, donc ERROR
                    state = State.ERROR;
                    return ProcessStatus.ERROR;
                }
                state = State.GOT_SIZE; // ta réussi à récupérer la size, GOOD.
                buffer = ByteBuffer.allocate( Integer.BYTES + sizeData );
                buffer.putInt( sizeData );
            case GOT_SIZE:
                bb.flip();
                try {
                    if (bb.remaining() <= internalbb.remaining() ){
                        internalbb.put(bb);
                    } else {
                        var oldLimit = bb.limit();
                        bb.limit(internalbb.remaining());
                        internalbb.put(bb);
                        bb.limit(oldLimit);
                    }
                } finally {
                    bb.compact();
                }
                //
                internalbb.flip();
                buffer.put(internalbb);
                internalbb.clear();
                // Read string
                if( buffer.hasRemaining() ) {
                    return ProcessStatus.REFILL;
                }
                buffer.flip();
                privatePacket = new PrivatePacket(buffer);
                state = State.DONE;
                return ProcessStatus.DONE;
            default:
                throw new IllegalStateException();
        }
    }

    /**
     * Get the bytebuffer in read-mode
     * @return bytebuffer containing the values
     */
    @Override
    public PrivatePacket get() {
        if ( state != state.DONE) {
            throw new IllegalStateException();
        }
        return privatePacket;
    }

    /**
     * Reset the process
     */
    @Override
    public void reset() {
        state = State.NEXT;
        internalbb.clear();
        intReader.reset();
    }
}
