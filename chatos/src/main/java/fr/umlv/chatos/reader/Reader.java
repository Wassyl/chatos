package fr.umlv.chatos.reader;

import java.nio.ByteBuffer;

public interface Reader<T> {

    /**
     * Enumeration of state
     */
    enum ProcessStatus {DONE, REFILL, ERROR};

    /**
     * Process the bytebuffer according to each reader
     * @param bb
     * @return
     */
    ProcessStatus process(ByteBuffer bb);

    /**
     * Get method to get the value of each reader
     * @return the value of each reader
     */
    T get();

    /**
     * Reset the process
     */
    void reset();
}
