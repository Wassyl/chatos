package fr.umlv.chatos.packet;

import fr.umlv.chatos.visitor.FrameVisitor;

import java.nio.ByteBuffer;

public class PrivatePacket implements Frame {

    private final ByteBuffer bb;

    public PrivatePacket(ByteBuffer bb) {
        this.bb = bb;
    }

    @Override
    public ByteBuffer asByteBuffer() {
        return bb.duplicate();
    }

    @Override
    public void accept(FrameVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return "Received " + bb.remaining() + " bytes";
    }
}
