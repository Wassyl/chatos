package fr.umlv.chatos.packet;

import fr.umlv.chatos.visitor.FrameVisitor;

import java.nio.ByteBuffer;

/**
 * Operand Established
 */
public class Established implements Frame {

    /**
     * Create a operand Established
     */
    private final Operands opcode = Operands.ESTABLISHED;

    /**
     * Tranform into a bytebuffer the operand
     * @return the bytebuffer of the operand
     */
    @Override
    public ByteBuffer asByteBuffer() {
        return ByteBuffer.allocate(1).put(opcode.value()).flip();
    }

    @Override
    public void accept(FrameVisitor visitor) {
        visitor.visit(this);
    }

}