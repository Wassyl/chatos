package fr.umlv.chatos.packet;

import fr.umlv.chatos.visitor.FrameVisitor;

import java.nio.ByteBuffer;

/**
 * Operand LoginPrivate
 */
public class LoginPrivate implements Frame {

    /**
     * Operand of Login_PRIVATE
     */
    private final Operands opcode = Operands.LOGIN_PRIVATE;

    /**
     * The ConnectId for the private connection
     */
    private final long connectId;

    /**
     * Initialize the LoginPrivate
     * @param connectId
     */
    public LoginPrivate(long connectId) {
        this.connectId = connectId;
    }

    /**
     * Transform into the bytebuffer the operand
     * @return the bytebuffer of the operand
     */
    @Override
    public ByteBuffer asByteBuffer() {
        return ByteBuffer.allocate(9).put(opcode.value()).putLong(connectId).flip();
    }

    @Override
    public void accept(FrameVisitor visitor) {
        visitor.visit(this);
    }

    /**
     * Get the connectId
     * @return connectId
     */
    public long getConnectId(){
        return connectId;
    }

}
