package fr.umlv.chatos.packet;

import fr.umlv.chatos.visitor.FrameVisitor;

import java.nio.ByteBuffer;

/**
 * Operand Message
 */
public class Message implements Frame {

    /**
     * Operand of Message
     */
    private final Operands opcode = Operands.MESSAGE;

    /**
     * The requester
     */
    private final String requester;

    /**
     * The message
     */
    private final String message;

    /**
     * Initialize the message
     * @param requester
     * @param message
     */
    public Message(String requester, String message) {
        this.requester = requester;
        this.message = message;
    }

    /**
     * Transform into the bytebuffer the operand
     * @return the bytebuffer of the operand
     */
    @Override
    public ByteBuffer asByteBuffer() {
        var sender = UTF_8.encode(this.requester);
        var message = UTF_8.encode(this.message);
        var bb = ByteBuffer.allocate(Integer.BYTES*2 + sender.remaining() + message.remaining() + 1);
        bb.put(opcode.value());
        bb.putInt(sender.remaining());
        bb.put(sender);
        bb.putInt(message.remaining());
        bb.put(message);
        bb.flip();
        return bb;
    }

    /***
     * Print the message with its sender
     * @return the message with the sender
     */
    @Override
    public String toString() {
        return requester + ": " + message;
    }

    @Override
    public void accept(FrameVisitor visitor) {
        visitor.visit(this);
    }

    /**
     * Get the requester
     * @return requester
     */
    public String getRequester() {
        return requester;
    }

    /**
     * Get the message
     * @return message
     */
    public String getText() {
        return message;
    }
}
