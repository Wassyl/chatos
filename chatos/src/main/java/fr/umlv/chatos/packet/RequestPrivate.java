package fr.umlv.chatos.packet;

import fr.umlv.chatos.visitor.FrameVisitor;

import java.nio.ByteBuffer;

/**
 * Operand of Request Private
 */
public class RequestPrivate implements Frame {

    /**
     * Operand of Request Private
     */
    private final Operands opcode = Operands.REQUEST_PRIVATE;

    /**
     * The requester
     */
    private final String requester;

    /**
     * The target
     */
    private final String target;

    /**
     * Initialize the request private
     * @param requester
     * @param target
     */
    public RequestPrivate(String requester, String target) {
        this.requester = requester;
        this.target = target;
    }

    /**
     * Transform into the bytebuffer the operand
     * @return the bytebuffer of the operand
     */
    @Override
    public ByteBuffer asByteBuffer() {
        var sender = UTF_8.encode(this.requester);
        var target = UTF_8.encode(this.target);
        var bb = ByteBuffer.allocate(Integer.BYTES*2 + target.remaining() + sender.remaining() + 1);
        bb.put(opcode.value());
        bb.putInt(sender.remaining());
        bb.put(sender);
        bb.putInt(target.remaining());
        bb.put(target);
        bb.flip();
        return bb;
    }

    /**
     * A message to signal that the sender send a request for private connection
     * @return a message to signal that the sender send a request for private connection
     */
    @Override
    public String toString() {
        return requester + " has sent you a request for a private connection - To confirm, type in : /"+ requester + " OK";
    }

    @Override
    public void accept(FrameVisitor visitor) {
        visitor.visit(this);
    }

    /**
     * Get the requester
     * @return the requester
     */
    public String getRequester() {
        return requester;
    }

    /**
     * Get the target
     * @return the target
     */
    public String getTarget() {
        return target;
    }

}