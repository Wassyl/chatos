package fr.umlv.chatos.packet;

import fr.umlv.chatos.visitor.FrameVisitor;

import java.nio.ByteBuffer;

/**
 * Operand LoginRefused
 */
public class LoginRefused implements Frame {

    /**
     * Operand LoginRefused
     */
    private final Operands opcode = Operands.LOGIN_REFUSED;

    /**
     * Transform into the bytebuffer the operand
     * @return the bytebuffer of the operand
     */
    @Override
    public ByteBuffer asByteBuffer() {
        return ByteBuffer.allocate(1).put(opcode.value()).flip();
    }

    @Override
    public void accept(FrameVisitor visitor) {
        visitor.visit(this);
    }
}
