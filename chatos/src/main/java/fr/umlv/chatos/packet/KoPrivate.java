package fr.umlv.chatos.packet;

import fr.umlv.chatos.visitor.FrameVisitor;

import java.nio.ByteBuffer;

/**
 * Operand of KO_PRIVATE
 */
public class KoPrivate implements Frame {

    /**
     * Operand KO_PRIVATE
     */
    private final Operands opcode = Operands.KO_PRIVATE;

    /**
     * The requester
     */
    private final String requester;

    /**
     * The target
     */
    private final String target;

    /**
     * Initialize the KO_PRIVATE operand
     * @param requester
     * @param target
     */
    public KoPrivate(String requester, String target) {
        this.requester = requester;
        this.target = target;
    }

    /**
     * Transform into the bytebuffer the operand
     * @return the bytebuffer of the operand
     */
    @Override
    public ByteBuffer asByteBuffer() {
        var sender = UTF_8.encode(this.requester);
        var target = UTF_8.encode(this.target);
        var bb = ByteBuffer.allocate(Integer.BYTES*2 + target.remaining() + sender.remaining() + 1);
        bb.put(opcode.value());
        bb.putInt(sender.remaining());
        bb.put(sender);
        bb.putInt(target.remaining());
        bb.put(target);
        bb.flip();
        return bb;
    }

    /***
     * Print a message to reject the connection
     * @return a message to reject the connection
     */
    @Override
    public String toString() {
        return target + " has rejected your request for a private connection";
    }

    @Override
    public void accept(FrameVisitor visitor) {
        visitor.visit(this);
    }

    /**
     * Get the target
     * @return the target
     */
    public String getRequester() {
        return requester;
    }

    /**
     * get the connect id
     * @return the connect id
     */
    public String getTarget() {
        return target;
    }
}
