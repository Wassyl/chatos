package fr.umlv.chatos.packet;

import fr.umlv.chatos.visitor.FrameVisitor;

import java.nio.ByteBuffer;

/**
 * Operand LOGIN
 */
public class Login implements Frame {

    /**
     * Operand LOGIN
     */
    private final Operands opcode = Operands.LOGIN;

    /**
     * The requester
     */
    private final String requester;

    /**
     * Initialize the login
     * @param requester
     */
    public Login( String requester) {
        this.requester = requester;
    }

    /**
     * Transform into the bytebuffer the operand
     * @return the bytebuffer of the operand
     */
    @Override
    public ByteBuffer asByteBuffer() {
        var tmp = UTF_8.encode(requester);
        var bb = ByteBuffer.allocate(tmp.remaining() + Integer.BYTES + 1);
        bb.put(opcode.value()).putInt(tmp.remaining()).put(tmp).flip();
        return bb;
    }

    @Override
    public void accept(FrameVisitor visitor) {
        visitor.visit(this);
    }

    /**
     * Get the requester
     * @return the requester
     */
    public String getRequester() { return requester;}
}
