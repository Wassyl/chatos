package fr.umlv.chatos.packet;

import fr.umlv.chatos.visitor.FrameVisitor;

import java.nio.ByteBuffer;

/**
 * Operand of Message Private
 */
public class MessagePrivate implements Frame {

    /**
     * Operand of Message Private
     */
    private final Operands opcode = Operands.MESSAGE_PRIVATE;

    /**
     * The requester
     */
    private final String requester;

    /**
     * The target
     */
    private final String target;

    /**
     * The message
     */
    private final String message;

    /**
     * Initialize the message private
     * @param requester
     * @param target
     * @param message
     */
    public MessagePrivate(String requester, String target, String message) {
        this.requester = requester;
        this.target = target;
        this.message = message;
    }

    /**
     * Transform into the bytebuffer the operand
     * @return the bytebuffer of the operand
     */
    @Override
    public ByteBuffer asByteBuffer() {
        var sender = UTF_8.encode(this.requester);
        var target = UTF_8.encode(this.target);
        var message = UTF_8.encode(this.message);
        var bb = ByteBuffer.allocate(Integer.BYTES*3 + target.remaining() + sender.remaining() + message.remaining() + 1);
        bb.put(opcode.value());
        bb.putInt(sender.remaining());
        bb.put(sender);
        bb.putInt(target.remaining());
        bb.put(target);
        bb.putInt(message.remaining());
        bb.put(message);
        bb.flip();
        return bb;
    }

    /***
     * Print the message with its sender
     * @return the message with the sender
     */
    @Override
    public String toString() {
        return "(Private) " + requester + ": " + message;
    }

    @Override
    public void accept(FrameVisitor visitor) {
        visitor.visit(this);
    }

    /**
     * Get the requester
     * @return requester
     */
    public String getRequester() {
        return requester;
    }

    /**
     * Get the target
     * @return target
     */
    public String getTarget() {
        return target;
    }

    /**
     * Get the message
     * @return message
     */
    public String getText() { return message; }
}
