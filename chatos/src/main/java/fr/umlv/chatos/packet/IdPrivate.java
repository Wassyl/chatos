package fr.umlv.chatos.packet;

import fr.umlv.chatos.visitor.FrameVisitor;

import java.nio.ByteBuffer;

/**
 * Operand ID_PRIVATE
 */
public class IdPrivate implements Frame {

    /**
     * Operand of ID_PRIVATE
     */
    private final Operands opcode = Operands.ID_PRIVATE;

    /**
     * The requester
     */
    private final String requester;

    /**
     * The target
     */
    private final String target;

    /**
     * The connect Id : a long corresponding of the private connection between a sender and a target
     */
    private final long connectId;

    /**
     * Initialize a private id
     * @param requester
     * @param target
     * @param connectId
     */
    public IdPrivate(String requester, String target, long connectId) {
        this.requester = requester;
        this.target = target;
        this.connectId = connectId;
    }

    /**
     * Transform into the bytebuffer the operand
     * @return the bytebuffer of the operand
     */
    @Override
    public ByteBuffer asByteBuffer() {
        var sender = UTF_8.encode(this.requester);
        var target = UTF_8.encode(this.target);
        var bb = ByteBuffer.allocate(Integer.BYTES*2 + Long.BYTES + target.remaining() + sender.remaining() + 1);
        bb.put(opcode.value());
        bb.putInt(sender.remaining());
        bb.put(sender);
        bb.putInt(target.remaining());
        bb.put(target);
        bb.putLong(connectId);
        bb.flip();
        return bb;
    }

    /***
     * Print a message to confirm the connection between sender and target
     * @return a message to confirm the connection
     */
    @Override
    public String toString() { return "Connection between "+ requester +" and "+ target +" initialized with id : "+ connectId; }

    @Override
    public void accept(FrameVisitor visitor) {
        visitor.visit(this);
    }

    /**
     * Get the requester
     * @return requester
     */
    public String getRequester() {
        return requester;
    }

    /**
     * Get the target
     * @return the target
     */
    public String getTarget() { return target; }

    /**
     * get the connect id
     * @return the connect id
     */
    public long getConnectId(){
        return connectId;
    }

}
