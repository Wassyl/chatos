package fr.umlv.chatos.packet;

import fr.umlv.chatos.visitor.FrameVisitor;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Interface for all frames
 */
public interface Frame {

    /**
     * Define CHARSET UTF8
     */
    Charset UTF_8 = StandardCharsets.UTF_8;

    /**
     * Transform command to byteBuffer
     * @return bytebuffer
     */
    ByteBuffer asByteBuffer();


    void accept(FrameVisitor visitor);

    /**
     * Command to string
     * @return a string of the command
     */
    String toString();

    /**
     * get the operand of a byte
     * @param op
     * @return operand
     */
    static Operands getOperandsFor(byte op){
        return Operands.get(op);
    }

    /**
     * Transform a message into bytebuffer
     * @param message
     * @return bytebuffer of the message
     */
    static ByteBuffer convertToByteBuffer(String message) {
        var encoded = UTF_8.encode(message);
        var bb = ByteBuffer.allocate(Integer.BYTES + encoded.remaining());
        bb.putInt(encoded.remaining());
        bb.put(encoded);
        bb.flip();
        return bb;
    }

    /**
     * Enumeration of all operands
     */
    enum Operands {
        LOGIN((byte) 0),
        LOGIN_ACCEPTED((byte) 1),
        LOGIN_REFUSED((byte) 2),
        MESSAGE((byte) 3),
        MESSAGE_PRIVATE((byte) 4),
        REQUEST_PRIVATE((byte) 5),
        OK_PRIVATE((byte) 6),
        KO_PRIVATE((byte) 7),
        ID_PRIVATE((byte) 8),
        LOGIN_PRIVATE((byte) 9),
        ESTABLISHED((byte) 10);

        /**
         * Value of the operand in byte
         */
        private final byte value;

        /**
         * Initialize an operand with a value
         * @param value
         */
        Operands(byte value){
            this.value = value;
        }

        /**
         * @return the value of the operands
         */
        public byte value(){
            return value;
        }

        /**
         *
         * @param op
         * @return the corresponding operands for each values.
         */
        private static Operands get(byte op) {
            switch(op) {
                case  0: return LOGIN;
                case  1: return LOGIN_ACCEPTED;
                case  2: return LOGIN_REFUSED;
                case  3: return MESSAGE;
                case  4: return MESSAGE_PRIVATE;
                case  5: return REQUEST_PRIVATE;
                case  6: return OK_PRIVATE;
                case  7: return KO_PRIVATE;
                case  8: return ID_PRIVATE;
                case  9: return LOGIN_PRIVATE;
                case  10: return ESTABLISHED;
                default: throw new IllegalArgumentException("Unknown operand");
            }
        }
    }
}
