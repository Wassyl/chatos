package fr.umlv.chatos.server;

import fr.umlv.chatos.context.server.ServerPrivateContext;
import fr.umlv.chatos.context.server.ServerPublicContext;
import fr.umlv.chatos.packet.Established;
import fr.umlv.chatos.packet.PrivatePacket;

import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

class PrivateConnectionHandler {

    /**
     * Used to manage a private connection between two clients.
     */
    private static class PrivateConnection {

        private final SelectionKey[] keys = new SelectionKey[2];
        private final ServerPrivateContext[] contexts = new ServerPrivateContext[2];
        private int count = 0;

        /**
         * Update the class with a fresh new connection received
         * @param key The new key from the connection just created
         */
        private void addConnection(SelectionKey key) {
            if( count == 2 ){
                logger.severe("Ignoring client trying to log to private connection because it's full");
                return;
            }
            keys[count] = key;
            count++;
        }

        /**
         * Start the private connection between the clients.
         * Sends a Established packet after created ServerPrivateContext for both of them.
         * Also registers them to the ServerChatOS instance's selector.
         * @param server ServerChatOS instance
         * @param selector Server instance's selector
         * @param id Token for the private connection
         * @throws ClosedChannelException  If the selector fails
         */
        private void launch(ServerChatOS server, Selector selector, long id) throws ClosedChannelException {
            var established = new Established().asByteBuffer();
            for( var i = 0; i < keys.length ; i++) {
                var channel = keys[i].channel();
                var newKey = channel.register(selector, SelectionKey.OP_WRITE);
                contexts[i] = new ServerPrivateContext(server, newKey, id);
                newKey.attach( contexts[i] );
                keys[i] = newKey;

                // Established Opcode 10
                contexts[i].queueMessage( established.asReadOnlyBuffer() );
            }
            logger.info("Private connection with id n° " + id + " initiated");
        }

        /**
         * Closes both channels
         */
        private void silentlyClose() {
            Arrays.stream(contexts).forEach(ServerPrivateContext::silentlyClose);
        }

        /**
         * Relay packet to its target
         * @param serverPrivateContext Sender's context
         * @param privatePacket Sender's packet
         */
        private void sendPrivatePacketFrom(ServerPrivateContext serverPrivateContext, PrivatePacket privatePacket) {
            var target = serverPrivateContext.equals(contexts[0]) ? 1 : 0;
            contexts[target].queueMessage( privatePacket.asByteBuffer() );
        }

        /**
         * Return true if both clients registered
         * @return True if both clients registered
         */
        private boolean isReady() {
            return count == 2;
        }
    }

    private static final Logger logger = Logger.getLogger(PrivateConnectionHandler.class.getName());

    private final Map<Long, PrivateConnection> pendingConnections = new HashMap<>();
    private final Map<Long, PrivateConnection> connections = new HashMap<>();

    private final ServerChatOS server;
    private final Selector selector;

    /**
     * Needs the server instance and selector to generate private contexts
     * @param server ServerChatOS instance
     * @param selector Server's selector
     */
    PrivateConnectionHandler(ServerChatOS server, Selector selector) {
        this.server = server;
        this.selector = selector;
    }

    /**
     * Initializes a PrivateConnection with its Token id.
     * Verifies the Token isn't contained in pending connections and functioning private connections
     * @return Token generated
     */
    long preparePendingConnection() {
        var rand = new Random();
        while( true ) {
            var idPrivate = rand.nextLong();
            if( !pendingConnections.containsKey(idPrivate) && !connections.containsKey(idPrivate)){
                pendingConnections.put(idPrivate, new PrivateConnection());
                return idPrivate;
            }
        }
    }

    /**
     * Update the private connection instance with the specified token
     * @param id Token of the private connection
     * @param key New key of the private connection
     */
    void addNewConnection(long id, SelectionKey key) {
        // Check if token is known, else close connection
        if( !pendingConnections.containsKey(id) ){
            logger.warning("Received a login private with unknown IdPrivate. Closing connection.");
            ((ServerPublicContext) key.attachment()).silentlyClose();
        }
        // Update the PrivateConnection
        var pendingConnection = pendingConnections.get(id);
        pendingConnection.addConnection(key);
    }

    /**
     * Used in the selector loop of the server, updates and starts pending connections who are ready.
     * A pending connection is ready when both keys were recovered.
     */
    void processPendingConnections() {
        // Get ready connections -> both clients joined the server with their new connection
        var readyConnections = pendingConnections.entrySet().stream()
                .filter( e -> e.getValue().isReady() )
                .collect(Collectors.toList());
        if( readyConnections.isEmpty() ){
            return;
        }
        // Remove them from the waiting connections
        pendingConnections.entrySet().removeAll(readyConnections);
        // Start them with new context for both and send Established
        readyConnections.forEach( e -> {
            connections.put(e.getKey(), e.getValue());
            try {
                e.getValue().launch(server, selector, e.getKey());
            } catch (ClosedChannelException closedChannelException) {
                closePrivateConnectionFor(e.getKey());
                logger.severe("Couldn't initiate private connection with id : " + e.getKey() );
            }
        } );
    }

    /**
     * Send a private packet to target
     * @param serverPrivateContext Sender's context
     * @param privatePacket Sender's packet to transmit
     */
    void sendPrivatePacketFrom(ServerPrivateContext serverPrivateContext, PrivatePacket privatePacket) {
        connections.get(serverPrivateContext.getConnectId()).sendPrivatePacketFrom(serverPrivateContext, privatePacket);
    }

    /**
     * Shutdown both channels for each client and removes the token for the map
     * @param id Token for a private connection
     */
    void closePrivateConnectionFor(long id) {
        connections.get(id).silentlyClose();
        connections.remove(id);
    }
}
