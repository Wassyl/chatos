package fr.umlv.chatos.server;

import fr.umlv.chatos.context.server.ServerContext;
import fr.umlv.chatos.context.server.ServerPrivateContext;
import fr.umlv.chatos.context.server.ServerPublicContext;
import fr.umlv.chatos.packet.*;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.nio.channels.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Create the server for the Chat OS Protocol
 */
public class ServerChatOS {

    /**
     * Logger to announce info, warning or error
     */
    private static final Logger logger = Logger.getLogger(ServerChatOS.class.getName());

    /**
     * HashMap mapClients to store all the client connected to the server
     */
    private final HashMap<String, ServerContext> mapClients = new HashMap<>();

    /**
     * PrivateConnectionHandler instance
     */
    private final PrivateConnectionHandler privateConnectionHandler;

    /**
     * The thread Console
     */
    private final Thread console;

    /**
     * Lock used for the data race on commandQueue
     */
    private final Object lock = new Object();

    /**
     * A queue to store command
     */
    private final ArrayBlockingQueue<String> commandQueue = new ArrayBlockingQueue<>(10);

    /**
     * The ServerSocketChannel
     */
    private final ServerSocketChannel serverSocketChannel;

    /**
     * The selector
     */
    private final Selector selector;

    /**
     * Initialize the server for the Chat OS
     * @param port Server port
     * @throws IOException If selector.open() fails or if binding server socket channel fails
     */
    public ServerChatOS(int port) throws IOException {
        serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.bind(new InetSocketAddress(port));
        selector = Selector.open();
        console = new Thread(this::consoleRun);
        privateConnectionHandler = new PrivateConnectionHandler(this, selector);
    }

    /**
     * Method to launch the server
     * @throws IOException If binding server socket channel fails
     */
    public void launch() throws IOException {
        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        console.start();

        while(!Thread.interrupted()) {
            printKeys(); // for debug
            try {
                // Initiate private connections who are ready
                privateConnectionHandler.processPendingConnections();
                selector.select(this::treatKey);
                // Proceed console commands
                processCommands();
            } catch (UncheckedIOException tunneled) {
                throw tunneled.getCause();
            }
        }
    }

    /**
     * Try to read the console to know the command and send it
     */
    private void consoleRun() {
        try {
            var scan = new Scanner(System.in);
            while (scan.hasNextLine()) {
                var command = scan.nextLine();
                if( command.isEmpty() ){
                    continue;
                }
                sendCommand(command);
                if( command.equals("SHUTDOWNNOW") ){
                    return;
                }
            }
        } catch (InterruptedException e) {
            logger.info("Console thread has been interrupted");
        } finally {
            logger.info("Console thread stopping");
        }
    }

    /**
     * Send a command to the selector via commandQueue and wake it up
     *
     * @param command Command to execute
     * @throws InterruptedException If interrupted while putting in map
     */
    private void sendCommand(String command) throws InterruptedException {
        synchronized (lock) {
            commandQueue.put(command);
            selector.wakeup();
        }
    }

    /**
     * Processes the command from commandQueue
     */
    private void processCommands() throws IOException {
        synchronized (lock) {
            String command;
            while( (command = commandQueue.poll()) != null ) {
                switch (command){
                    case "INFO" -> showUsers();
                    case "SHUTDOWN" -> {
                        logger.info("Disallowing new clients");
                        serverSocketChannel.close(); // No more SelectionKey.OP_ACCEPT
                    }
                    case "SHUTDOWNNOW" -> close();
                    default -> logger.info("Unknown command");
                }
            }
        }
    }

    /**
     * Treat the SelectionKey
     * @param key Key to be treated by the selector
     */
    private void treatKey(SelectionKey key) {
        printSelectedKey(key); // for debug
        try {
            if (key.isValid() && key.isAcceptable()) {
                doAccept(key);
            }
        } catch(IOException ioe) {
            throw new UncheckedIOException(ioe);
        }
        try {
            if (key.isValid() && key.isWritable()) {
                ((ServerContext) key.attachment()).doWrite();
            }
            if (key.isValid() && key.isReadable()) {
                ((ServerContext) key.attachment()).doRead();
            }
        } catch (IOException e) {
            logger.log(Level.INFO,"Connection closed with client due to IOException",e);
            silentlyClose(key);
        }
    }

    /**
     * Accept the connection with the client
     * @param key Key client trying to connect
     * @throws IOException If server socket channel has an error
     */
    private void doAccept(SelectionKey key) throws IOException {
        var ssc = (ServerSocketChannel) key.channel();
        var sc = ssc.accept();
        if ( sc == null ){
            logger.info("Accept() : Socket channel is null");
            return;
        }
        sc.configureBlocking(false);
        var keyClient = sc.register(selector, SelectionKey.OP_READ);
        keyClient.attach(new ServerPublicContext(this, keyClient));
    }

    /**
     * Close a SocketChannel
     * @param key key linked to the socket channel being closed
     */
    public void silentlyClose(SelectionKey key) {
        var sc = (Channel) key.channel();
        try {
            sc.close();
        } catch (IOException e) {
            // ignore exception
        }
    }

    /**
     *
     * Add a client to the map to confirm the login in public connection
     * @param login Login of the client to be added
     * @param context Client context to add
     */
    public void addingNewClient(String login, ServerPublicContext context) {
        if( login.isEmpty() ) {
            logger.info("Closing user connection with empty login");
            context.queueMessage( new LoginRefused().asByteBuffer() );
            return;
        }
        if( stringIsTooLongUtf8(login, 30) ) {
            logger.info("Closing user connection : username is too long");
            context.queueMessage( new LoginRefused().asByteBuffer() );
            return;
        }
        logger.info("Adding "+ login +" to the list of users");
        // If the server couldn't add the login to its map, refuses the connection
        if( mapClients.containsKey(login) ) {
            logger.info("Refused a new client with username - " + login);
            context.queueMessage( new LoginRefused().asByteBuffer() );
            return;
        }
        // Update the context with the login - useful when removing the client from the map when client closes its connection
        context.setLogin(login);
        // Update the map of the clients
        mapClients.put(login, context);
        logger.info(login + " has joined the chat");
        // Notify client
        context.queueMessage( new LoginAccepted().asByteBuffer() );
    }

    /**
     * Remove the client from the map when the client disconnect with the server
     * @param login Login client to be removed
     */
    public void removeClientFromMap(String login) {
        mapClients.remove(login);
    }

    /**
     * Add a message to all connected clients queue
     * @param message Message packet to broadcast
     */
    public void broadcast(Message message) {
        // Ignore message if it is too long
        if( stringIsTooLongUtf8(message.getText(), 1024) ){
            logger.info("Ignoring message from "+ message.getRequester() +" for being too long");
            return;
        }
        if( !mapClients.containsKey(message.getRequester()) ){
            logger.warning("Ignoring message coming from unregistered client : " + message.getRequester());
            return;
        }
        var bb = message.asByteBuffer();
        mapClients.values().forEach(context -> context.queueMessage(bb.asReadOnlyBuffer()));
    }

    /**
     * Simple test if a message is too long to be transferred
     * @param string String message being tested
     * @param size Max number of bytes in UTF-8
     * @return If message has a length superior to 1024 bytes
     */
    private boolean stringIsTooLongUtf8(String string, int size) {
        var bb = StandardCharsets.UTF_8.encode(string);
        return bb.remaining() > size;
    }

    /**
     * Method to relay a private message to its target.
     * @param messagePrivate MessagePrivate packet being transmitted
     */
    public void privateMessageTo(MessagePrivate messagePrivate) {
        if( !mapClients.containsKey(messagePrivate.getRequester())
            || !mapClients.containsKey(messagePrivate.getTarget()) ){
            logger.warning("Unknown sender/target in private message : " + messagePrivate.getRequester() + " to " + messagePrivate.getTarget());
            return;
        }
        // Ignore message if it is too long
        if( stringIsTooLongUtf8(messagePrivate.getText(), 1024) ){
            logger.info("Ignoring message from "+ messagePrivate.getRequester() + " to " + messagePrivate.getTarget() + "for being too long");
            return;
        }
        // Check existence of context
        var context = mapClients.get( messagePrivate.getTarget() );
        if( context == null ){
            logger.severe(" Target doesn't have a context : " + messagePrivate.getTarget() );
            return;
        }
        context.queueMessage( messagePrivate.asByteBuffer() );
    }

    /**
     * Relaying the private command
     * @param requestPrivate RequestPrivate packet transmitted to its target
     */
    public void requestPrivateTo(RequestPrivate requestPrivate) {
        if( !mapClients.containsKey(requestPrivate.getRequester())
                || !mapClients.containsKey(requestPrivate.getTarget()) ){
            logger.warning("Unknown sender/target in private request : " + requestPrivate.getRequester() + " to " + requestPrivate.getTarget());
            return;
        }
        var context = mapClients.get(requestPrivate.getTarget());
        if( context == null ){
            logger.severe(" Target doesn't have a context : " + requestPrivate.getTarget() );
            return;
        }
        context.queueMessage( requestPrivate.asByteBuffer() );
    }

    /**
     * Send a KoPrivate packet to its target
     * @param koPrivate KoPrivate packet transmitted to its target
     */
    public void koPrivateTo(KoPrivate koPrivate) {
        if( !mapClients.containsKey(koPrivate.getRequester())
                || !mapClients.containsKey(koPrivate.getTarget()) ){
            logger.warning("Unknown sender/target in ko private : " + koPrivate.getRequester() + " to " + koPrivate.getTarget());
            return;
        }
        var context = mapClients.get(koPrivate.getTarget());
        if( context == null ){
            logger.severe(" Target doesn't have a context : " + koPrivate.getTarget() );
            return;
        }
        context.queueMessage( koPrivate.asByteBuffer() );
    }

    /**
     * Send the IdPrivate to the requester and the target
     * @param requester Login requester
     * @param target Login target
     */
    public void confirmPrivateConnection(String requester, String target) {
        if( !mapClients.containsKey( requester)
                || !mapClients.containsKey( target ) ){
            logger.warning("Unknown sender/target in okPrivate : " + requester + " to " + target);
            return;
        }

        // Recover a token who doesn't exist
        var connectId = privateConnectionHandler.preparePendingConnection();

        var bbIdPrivate = new IdPrivate(requester, target, connectId).asByteBuffer();
        // Send IdPrivate - OPCODE(8)
        mapClients.get( requester ).queueMessage( bbIdPrivate.asReadOnlyBuffer() ); // Read-only
        mapClients.get( target ).queueMessage( bbIdPrivate.asReadOnlyBuffer() ); // Read-only

        logger.info("Private connection - "+ target +" has accepted a private connection from "+ requester);
    }

    /**
     * Handling a new connection which is attached to a public context for the time being.
     * Will update a PrivateConnection update in the
     * @param id Token received from LoginPrivate packet
     * @param ctx ServerPublicContext attached to the connection
     */
    public void handleLoginPrivate(long id, ServerPublicContext ctx) {
        privateConnectionHandler.addNewConnection(id, ctx.getKey());
    }

    /**
     * Relay a private packet of a private connection to its target
     * @param serverPrivateContext Sender's context
     * @param privatePacket Sender's packet
     */
    public void sendPrivatePacketFrom(ServerPrivateContext serverPrivateContext, PrivatePacket privatePacket) {
        privateConnectionHandler.sendPrivatePacketFrom(serverPrivateContext, privatePacket);
    }

    /**
     * Close the privateConnection for two clients with the semae connectId
     * @param connectId Token linking both client contexts
     */
    public void closePrivateConnectionFor(long connectId) {
        logger.info("Closing private connection - id n°" + connectId);
        privateConnectionHandler.closePrivateConnectionFor(connectId);
    }

    /**
     * Show how much users are connected to the server
     */
    private void showUsers() {
        var nb = selector.keys()
                .stream()
                .filter(k -> k != serverSocketChannel.keyFor(selector))
                .count();
        logger.info("Current connected users : " + nb);
    }

    /**
     * Close the server and all clients connect to it
     * @throws IOException If selector throws
     */
    private void close() throws IOException {
        logger.info("Closing server");
        selector.keys().forEach(this::silentlyClose);
        selector.close();
        Thread.currentThread().interrupt();
    }

    /**
     * Main to launch the server
     * @param args Port server
     * @throws IOException If the server fails
     */
    public static void main(String[] args) throws IOException {
        if (args.length!=1){
            usage();
            return;
        }
        try {
            var port = Integer.parseInt(args[0]);
            // Start server
            var server = new ServerChatOS(port);
            server.launch();
        } catch (NumberFormatException nbe) {
            usage();
        }
    }

    /**
     * How to launch the server
     */
    private static void usage(){
        System.out.println("Usage : ServerChatOS port");
    }

    /***
     *  Theses methods are here to help understanding the behavior of the selector
     ***/

    private String interestOpsToString(SelectionKey key){
        if (!key.isValid()) {
            return "CANCELLED";
        }
        int interestOps = key.interestOps();
        ArrayList<String> list = new ArrayList<>();
        if ((interestOps&SelectionKey.OP_ACCEPT)!=0) list.add("OP_ACCEPT");
        if ((interestOps&SelectionKey.OP_READ)!=0) list.add("OP_READ");
        if ((interestOps&SelectionKey.OP_WRITE)!=0) list.add("OP_WRITE");
        return String.join("|",list);
    }

    /**
     * print keys of the selector according if is the server socket channel or the client.
     */
    public void printKeys() {
        Set<SelectionKey> selectionKeySet = selector.keys();
        if (selectionKeySet.isEmpty()) {
            System.out.println("The selector contains no key : this should not happen!");
            return;
        }
        System.out.println("The selector contains:");
        for (SelectionKey key : selectionKeySet){
            SelectableChannel channel = key.channel();
            if (channel instanceof ServerSocketChannel) {
                System.out.println("\tKey for ServerSocketChannel : "+ interestOpsToString(key));
            } else {
                SocketChannel sc = (SocketChannel) channel;
                System.out.println("\tKey for Client "+ remoteAddressToString(sc) +" : "+ interestOpsToString(key));
            }
        }
    }

    /**
     * transform the remote address to string
     * @param sc SocketChannel target
     * @return a string of the remote address
     */
    private String remoteAddressToString(SocketChannel sc) {
        try {
            return sc.getRemoteAddress().toString();
        } catch (IOException e){
            return "???";
        }
    }

    /**
     * Print the key of the channel according if is the server socket channel or the client
     * @param key Key to print
     */
    public void printSelectedKey(SelectionKey key) {
        SelectableChannel channel = key.channel();
        if (channel instanceof ServerSocketChannel) {
            System.out.println("\tServerSocketChannel can perform : " + possibleActionsToString(key));
        } else {
            SocketChannel sc = (SocketChannel) channel;
            System.out.println("\tClient " + remoteAddressToString(sc) + " can perform : " + possibleActionsToString(key));
        }
    }

    /**
     * String of possible actions according to the selection key
     * @param key Key to show
     * @return string of possible actions according to the selection key
     */
    private String possibleActionsToString(SelectionKey key) {
        if (!key.isValid()) {
            return "CANCELLED";
        }
        ArrayList<String> list = new ArrayList<>();
        if (key.isAcceptable()) list.add("ACCEPT");
        if (key.isReadable()) list.add("READ");
        if (key.isWritable()) list.add("WRITE");
        return String.join(" and ",list);
    }
}
